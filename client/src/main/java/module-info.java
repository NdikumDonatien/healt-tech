module org.riser.corp.client {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;

    requires org.apache.logging.log4j;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires java.sql;

    opens org.riser.corp.client to javafx.fxml;
    opens org.riser.corp.client.controller to javafx.fxml;

    exports org.riser.corp.client;
    exports org.riser.corp.client.management.request.launcher;
    exports org.riser.corp.client.management.stream.parcel;
    exports org.riser.corp.client.management.stream.dictionary;
    exports org.riser.corp.client.dto;
    exports org.riser.corp.client.controller;

    opens org.riser.corp.client.dto.emergencies to com.fasterxml.jackson.databind, javafx.base;
    opens org.riser.corp.client.dto.ambulances to com.fasterxml.jackson.databind, javafx.base;
}