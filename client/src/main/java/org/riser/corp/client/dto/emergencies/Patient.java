package org.riser.corp.client.dto.emergencies;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.riser.corp.client.dto.DTO;

import java.time.LocalDate;


/*Hospital emergencies gives treats patients. However, it's not our topic to manage emergencies, so we will
limit ourselves to basic information about a patient*/
public class Patient extends DTO {
    private String secNumber;
    private LocalDate admissionDate;
    private LocalDate releaseDate;

    public Patient() {
    }

    public Patient(String security_number) {
        this.secNumber = security_number;
    }

    public String getSecNumber() {
        return secNumber;
    }

    public void setSecNumber(String secNumber) {
        this.secNumber = secNumber;
    }

    public LocalDate getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(LocalDate admissionDate) {
        this.admissionDate = admissionDate;
    }

    public LocalDate getReleaseDate() {
        return this.releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "secNumber='" + secNumber + '\'' +
                ", admissionDate='" + admissionDate + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", id=" + id +
                '}';
    }

}
