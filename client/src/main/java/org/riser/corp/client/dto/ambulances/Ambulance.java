package org.riser.corp.client.dto.ambulances;


import org.riser.corp.client.dto.DTO;

import java.time.LocalDate;

public class Ambulance extends DTO {
    private Integer id;
    private String registration;
    private String name;
    private String state;
    private String type;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;

    public Ambulance() {
    }
    public Ambulance(String registration) {
        this.registration = registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getRegistration() {
        return registration;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }
}
