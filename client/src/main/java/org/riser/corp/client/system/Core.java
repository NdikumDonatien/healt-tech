package org.riser.corp.client.system;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class Core {
    private static Socket serverSocket;
    public static BufferedReader READER;
    public static BufferedWriter WRITER;
    final static Logger LOGGER = LogManager.getLogger(Core.class);
    public Core() {
        try {
            serverSocket = new Socket("localhost", 9999);

            READER = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
            WRITER = new BufferedWriter(new OutputStreamWriter(serverSocket.getOutputStream()));
        }catch(IOException e){
            LOGGER.info("Impossible to reach sever : " + serverSocket.getInetAddress().getHostAddress() + " due to : " + e.getMessage() );
        }
    }


}
