package org.riser.corp.client.dto.emergencies;


import org.riser.corp.client.dto.DTO;

import java.time.LocalDate;


//Event known with predicted increase effect
public class Event extends DTO {
    private String name;
    private LocalDate beginDate;
    private LocalDate endDate;
    private Double effect;

    public Event() {
    }

    public Event(String name, LocalDate beginDate, LocalDate endDate, Double effect) {
        this.name = name;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.effect = effect;
    }

    public Event(String name, LocalDate beginDate, Double effect) {
        this.name = name;
        this.beginDate = beginDate;
        this.effect = effect;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Double getEffect() {
        return effect;
    }

    public void setEffect(Double effect) {
        this.effect = effect;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", effect=" + effect +
                ", id=" + id +
                '}';
    }
}
