package org.riser.corp.client.management.stream.parcel;

import org.riser.corp.client.management.stream.dictionary.ParcelDictionary.UseCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//Request is the pattern to build every request sent to the server
public class Request extends Parcel {
    final static Logger LOGGER = LogManager.getLogger(Request.class);
    public Request(){}
    public Request(UseCase useCase, String method, Object attribute) {
        super (useCase, method, attribute);
    }

    public Request(UseCase useCase) {
        this.setUseCase(useCase);
    }

    public Request(UseCase useCase, String method) {
        super(useCase, method);
    }
}

