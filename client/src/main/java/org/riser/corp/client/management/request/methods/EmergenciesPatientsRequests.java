package org.riser.corp.client.management.request.methods;

import org.riser.corp.client.dto.emergencies.Patient;
import org.riser.corp.client.management.stream.dictionary.ParcelDictionary;
import org.riser.corp.client.management.stream.parcel.Request;
import org.riser.corp.client.management.stream.parcel.Response;

import java.util.List;

import static org.riser.corp.client.management.request.launcher.RequestLauncher.LaunchRequest;
import static org.riser.corp.client.management.stream.parcel.ParcelParser.mapListObject;
import static org.riser.corp.client.management.stream.parcel.ParcelParser.mapObject;

public interface EmergenciesPatientsRequests {
    static List<Patient> getAllPatient(){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY, "GET_ALL_PATIENTS");
        Response response = LaunchRequest(request);
        List<Patient> patients = mapListObject(response.getObject(), Patient.class);
        return patients;
    };
    static Patient admitPatient(Patient patient){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY, "ADMIT_PATIENT", patient);
        Response response = LaunchRequest(request);
        patient = mapObject(response.getObject(), Patient.class);
        return patient;
    };

    static Patient releasePatient(Patient patient){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY, "RELEASE_PATIENT", patient);
        Response response = LaunchRequest(request);
        patient = mapObject(response.getObject(), Patient.class);
        return patient;
    };

}
