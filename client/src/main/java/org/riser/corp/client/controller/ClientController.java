package org.riser.corp.client.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.riser.corp.client.system.Core;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import static org.riser.corp.client.ClientApplication.switchToScene;

public class ClientController implements Initializable {
    final static Logger LOGGER = LogManager.getLogger(ClientController.class);
    @FXML
    private ComboBox<String> profile;
    @FXML
    private TextField identifier;
    @FXML
    private PasswordField password;
    @FXML
    private Button identificationButton;

    public static String pofileValue;
    private boolean connection = false;
    private String[] profiles = {"Direction", "Service Logistique", "Urgences"};
    private boolean valid = true;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        profile.getItems ().addAll (profiles);
    }

    public void submit(ActionEvent event) throws IOException {
        try {
            if(!connection){
                new Core();
                connection = !connection;
            }
        }catch(Exception e){
            LOGGER.error(e.getMessage());
        }
        LOGGER.info ("Profile : " + profile.getValue ());
        LOGGER.info ("Identifiant : " + identifier.getText ());
        LOGGER.info ("Password : " + password.getText ());
        pofileValue = profile.getValue();
        if(valid){
        try {
            if (profile.getValue ().equals ("Urgences")) {
                switchToEmergency(event);
            } else if (profile.getValue ().equals ("Service Logistique")) {
                switchToAmbulances (event);
            }
        }catch (NullPointerException e){

        }}
    }

    public void switchToEmergency(ActionEvent event) throws IOException {
        switchToScene (event, "emergency-patients.fxml");
    }
    public void switchToAmbulances(ActionEvent event) throws IOException {
        switchToScene(event, "ambulances.fxml");
    }
    public static void logOut(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Deconnection!");
        alert.setHeaderText("Vous allez vous deconnecter!");
        alert.setContentText("Êtes-vous sûr de vouloir continuer ?");
        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent() && result.get() == ButtonType.OK) {
            switchToScene (event, "client.fxml");
        }
    }
}