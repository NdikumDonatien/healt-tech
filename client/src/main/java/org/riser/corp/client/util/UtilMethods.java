package org.riser.corp.client.util;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.util.Duration;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface UtilMethods {
     static double roundDouble(double number, int decimals) {
        BigDecimal rounded = new BigDecimal(number).setScale(decimals, RoundingMode.HALF_UP);
        return rounded.doubleValue();
    }
    //Double r = Math.round(risk * 100)/100.0;
     static double roundDouble(double number){
        return roundDouble(number, 2);
    }

    static void setErrorLabel(Label label, String errorLabel, int s){
        label.setText(errorLabel);

        Timeline timeline = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            label.setVisible(true);
        }), new KeyFrame(Duration.seconds(s), e -> {
            label.setVisible(false);
        }));
        timeline.playFromStart();
    }
    static void setErrorLabel(Label label, String errorLabel){
        setErrorLabel(label, errorLabel, 7);
    }
}
