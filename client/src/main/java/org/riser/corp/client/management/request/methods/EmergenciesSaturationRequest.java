package org.riser.corp.client.management.request.methods;

import org.riser.corp.client.dto.emergencies.Event;
import org.riser.corp.client.management.stream.dictionary.ParcelDictionary;
import org.riser.corp.client.management.stream.parcel.Request;
import org.riser.corp.client.management.stream.parcel.Response;

import java.util.List;

import static org.riser.corp.client.management.request.launcher.RequestLauncher.LaunchRequest;
import static org.riser.corp.client.management.stream.parcel.ParcelParser.mapListObject;
import static org.riser.corp.client.management.stream.parcel.ParcelParser.mapObject;

public interface EmergenciesSaturationRequest {
    static Integer getEmergencyCapacity(){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY);
        request.setMethod("GET_EMERGENCY_CAPACITY");
        Response response = LaunchRequest(request);
        Integer emergencyCapacity = mapObject(response.getObject(), Integer.class);
        return emergencyCapacity;
    }

    static Integer getCurrentPatient(){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY);
        request.setMethod("GET_CURRENT_PATIENTS");
        Response response = LaunchRequest(request);
        Integer dayPatients = mapObject(response.getObject(), Integer.class);
        return dayPatients;
    }

    static Double getDaySaturation(){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY);
        request.setMethod("GET_DAY_SATURATION");
        Response response = LaunchRequest(request);
        Double dayRisk = mapObject(response.getObject(), Double.class);
        return dayRisk;
    }

    static Double getNextDaySaturation(){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY);
        request.setMethod("GET_NEXT_DAY_SATURATION");
        Response response = LaunchRequest(request);
        Double saturation = mapObject(response.getObject(), Double.class);
        return saturation;
    }
    static Double getNextWeekSaturation(){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY);
        request.setMethod("GET_NEXT_WEEK_SATURATION");
        Response response = LaunchRequest(request);
        Double saturation = mapObject(response.getObject(), Double.class);
        return saturation;
    }
    static Double getNextMonthSaturation(){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY);
        request.setMethod("GET_NEXT_MONTH_SATURATION");
        Response response = LaunchRequest(request);
        Double saturation = mapObject(response.getObject(), Double.class);
        return saturation;
    }

    static List<Integer> getDayHistory() {
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY);
        request.setMethod("GET_DAY_HISTORY");
        Response response = LaunchRequest(request);
        List<Integer> history = mapListObject(response.getObject(), Integer.class);
        return history;
    }
    static List<Event> getUpComingEvents(){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY);
        request.setMethod("GET_UPCOMING_EVENTS");
        Response response = LaunchRequest(request);
        List<Event> events = mapListObject(response.getObject(), Event.class);
        return events;
    }
    static Event registerEvent(Event event){
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY, "REGISTER_EVENT", event);
        Response response = LaunchRequest(request);
        event = mapObject(response.getObject(), Event.class);;
        return event;
    }
    static Boolean deleteEvent(Event event) {
        Request request = new Request(ParcelDictionary.UseCase.EMERGENCY, "DELETE_EVENT", event);
        Response response = LaunchRequest(request);
        Boolean res = mapObject(response.getObject(), Boolean.class);
        return res;
    }
}
