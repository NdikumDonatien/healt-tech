package org.riser.corp.client.management.request.methods.ambulances;

import org.riser.corp.client.dto.ambulances.Ambulance;
import org.riser.corp.client.management.stream.dictionary.ParcelDictionary;
import org.riser.corp.client.management.stream.parcel.Request;
import org.riser.corp.client.management.stream.parcel.Response;

import static org.riser.corp.client.management.request.launcher.RequestLauncher.LaunchRequest;
import static org.riser.corp.client.management.stream.dictionary.ParcelDictionary.*;

public interface AmbulanceRequest {

    static Response insertAmbulance(Ambulance ambulance){
        Request request = new Request(ParcelDictionary.UseCase.AMBULANCE, INSERT, ambulance);
        return LaunchRequest(request);
    }
    static Response updateAmbulance(Ambulance ambulance){
        Request request = new Request(ParcelDictionary.UseCase.AMBULANCE, UPDATE, ambulance);
        return LaunchRequest(request);
    }
    static Response printAllAmbulances(){
        return LaunchRequest(new Request(ParcelDictionary.UseCase.AMBULANCE, PRINT_ALL));
    }
}
