package org.riser.corp.client.util;

import java.lang.reflect.Field;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateConverter {
    public static <T> T convertDatesFormat(T object, String format) {
        try {
            Class<?> clazz = object.getClass();
            SimpleDateFormat sdf = new SimpleDateFormat(format);

            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);

                if (field.getType() == Date.class) {
                    Date dateValue = (Date) field.get(object);
                    if (dateValue != null) {
                        field.set(object, sdf.parse(sdf.format(dateValue)));
                    }
                } else if (field.getType() == LocalDate.class) {
                    LocalDate localDateValue = (LocalDate) field.get(object);
                    if (localDateValue != null) {
                        String formattedDate = sdf.format(java.sql.Date.valueOf(localDateValue));
                        LocalDate parsedDate = LocalDate.parse(formattedDate, DateTimeFormatter.ofPattern(format));
                        field.set(object, parsedDate);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

}

