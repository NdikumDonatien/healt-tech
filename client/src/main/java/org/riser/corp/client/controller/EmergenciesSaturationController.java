package org.riser.corp.client.controller;

import org.riser.corp.client.dto.emergencies.Event;
import javafx.collections.FXCollections;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import static org.riser.corp.client.ClientApplication.switchToScene;
import static org.riser.corp.client.management.request.methods.EmergenciesSaturationRequest.*;
import static org.riser.corp.client.controller.ClientController.*;
import static org.riser.corp.client.util.UtilMethods.roundDouble;
import static org.riser.corp.client.util.UtilMethods.setErrorLabel;

public class EmergenciesSaturationController implements Initializable{
    final static Logger LOGGER = LogManager.getLogger(EmergenciesSaturationController.class);
    public void switchToEmergency(ActionEvent event) throws IOException {
        switchToScene (event, "emergency-patients.fxml");
    }
    public void switchToLog(ActionEvent event) throws IOException {
        logOut(event);
    }

    @FXML
    private ComboBox predictOptions;
    @FXML
    private LineChart<?, ?> chart;
    @FXML
    private ProgressBar riskBar, satBar;
    @FXML
    private Label pLabel, sLabel, newEventErrorLabel, deleteEventErrorLabel, profile;
    @FXML
    private TextField nameField, effectField;
    @FXML
    private DatePicker endField, beginField;
    @FXML
    private Pane eventPane;
    @FXML
    private TableView<Event> eventTable;
    @FXML
    private TableColumn<Event, String> name;
    @FXML
    private TableColumn<Event, Date> beginDate, endDate;
    @FXML
    private TableColumn<Event, Double> effect;

    private Integer emergencyCapacity, dayPatients;
    private Double daySaturation;
    private ObservableList<Event> eventsList;
    private String[] options = {"du jour", "du lendemain", "de la semaine prochine", "du mois prochain"};

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        profile.setText(pofileValue);

        // add option to predict option
        predictOptions.getItems().addAll(options);

        //initializing DatePickers
        endField.setValue(LocalDate.now());
        beginField.setValue(LocalDate.now());

        //getting capacity
        try {
            chargeCapacity();
        }catch(Exception e){}
        //getting day risk
        try {
            chargeDayRisk();
        }catch(Exception e){}
        //getting day patients
        try {
            chargeCurrentPatient();
        }catch(Exception e){}
        //setting bars
        try {
            setBars();
        }catch(Exception e){}

        //Initializing chart
        LOGGER.info("initializing chart");
        chart.getYAxis().setAutoRanging(false);
        chart.setAnimated(false);

        //getting chart values
        try {
            chargeChart();
        }catch(Exception e){

        }

        //setting columns value factory
        name.setCellValueFactory (new PropertyValueFactory<>("name"));
        beginDate.setCellValueFactory (new PropertyValueFactory<> ("beginDate"));
        endDate.setCellValueFactory (new PropertyValueFactory<> ("endDate"));
        effect.setCellValueFactory (new PropertyValueFactory<> ("effect"));
        LOGGER.info("Value factories set");
        //adding data
        eventsList = FXCollections.observableArrayList();

        try {
            chargeEventsTable();
        }catch(Exception e){

        }

    }

    private void chargeCapacity(){
        emergencyCapacity = getEmergencyCapacity();
    }
    private void chargeDayRisk(){
        daySaturation = getDaySaturation();
    }
    private void chargeCurrentPatient(){
        dayPatients = getCurrentPatient();
    }

    private Double toRisk(Double saturation){
        Double risk = saturation /Double.valueOf(emergencyCapacity);
        return roundDouble(risk);
    }
    private void setBars(){
        Double risk = toRisk(daySaturation);
        Double saturation = toRisk(Double.valueOf(dayPatients));
        setRiskBar(risk);
        setSatBar(saturation);
    }
    private void chargeEventsTable(){
        List<Event> events = getUpComingEvents();
        for (Event event : events){
            eventsList.add (event);
        }
        LOGGER.info(eventsList);
    }
    private void chargeChart(){
        List<Integer> integers = getDayHistory();
        XYChart.Series series1 = new XYChart.Series<>();
        XYChart.Series series2 = new XYChart.Series<>();
        LocalDate date = LocalDate.now().minusDays(integers.size());
        int i = 0;
        series2.getData().add(new XYChart.Data(date.toString(), emergencyCapacity));
        for (int v : integers) {
            LOGGER.info("charging series");
            series1.getData().add(new XYChart.Data(date.toString(), v));
            i++;
            date = LocalDate.now().minusDays(integers.size() - i);
        }
        series2.getData().add(new XYChart.Data(date.toString(), emergencyCapacity));
        LOGGER.info("charging chart");
        chart.getData().add(series1);
        chart.getData().add(series2);
        LOGGER.info("chart charged");
    }

    public static void setProgress(Double prediction, ProgressBar bar, Label label){
        if(prediction < 0.6){
            bar.setStyle("-fx-accent: green;");
        } else if(prediction < 0.9) {
            bar.setStyle("-fx-accent: yellow;");
        } else if(prediction >= 0.9) {
            bar.setStyle("-fx-accent: orange;");
        }
        bar.setProgress(prediction);
        label.setText(prediction * 100 + "%");
    }
    private void setRiskBar(Double risk){
        setProgress(risk, riskBar, pLabel);
    }
    private void setSatBar(Double risk){
        setProgress(risk, satBar, sLabel);
    }
    private boolean shev = true;
    public void showEvent(ActionEvent event){
        eventTable.setItems (eventsList);
        if(shev){
        eventPane.setVisible(true);
        shev = false;
        }
        else {
            eventPane.setVisible(false);
            shev = true;
        }
    }
    public void hideEvent(ActionEvent event){
        eventPane.setVisible(false);
        shev = true;
    }

    public void createEvent(ActionEvent event) {
        LOGGER.info("Attempt to create an event");
        String name = nameField.getText().trim();
        LocalDate beginDate = beginField.getValue();
        LocalDate endDate = endField.getValue();
        String effectText = effectField.getText().trim();
        Double effect;

        // Verify if name is given
        if (name.isEmpty()) {
            setErrorLabel(newEventErrorLabel,"Le nom de l'événement doit être renseigné");
            return;
        }
        // Verify if begin date is given and valid
        if (beginDate == null) {
            setErrorLabel(newEventErrorLabel,"La date de début doit être renseignée");
            return;
        } else if (beginDate.isBefore(LocalDate.now())) {
            setErrorLabel(newEventErrorLabel,"La date de début doit être postérieure à la date courante");
            return;
        }
        // Verify if end date is valid
        if (endDate != null) {
             if (endDate.isBefore(beginDate)) {
                setErrorLabel(newEventErrorLabel,"La date de fin doit être postérieure à la date de début");
                return;
            }
        }
        // Verify if effect is a double and valid
        try {
            effect = Double.parseDouble(effectText);
            if(effect > 5.1){
                setErrorLabel(newEventErrorLabel,"L'effet doit être inférieur à 5.1");
                return;
            }else if(effect < -4.1){
                setErrorLabel(newEventErrorLabel,"L'effet doit être supérieur à -4.1");
                return;
            }
        } catch (NumberFormatException e) {
            setErrorLabel(newEventErrorLabel,"L'effet doit être un nombre décimal");
            return;
        }

        // Creation of the event
        Event newEvent = new Event(name, beginDate, endDate, effect);
        newEvent = registerEvent(newEvent);

        //add event in events
        eventsList.add(newEvent);
        eventTable.setItems (eventsList);
    }

    public void chosePrediction(ActionEvent event){
        String item = predictOptions.getSelectionModel().getSelectedItem().toString();
        Double risk = 0.0;
        if(item.equals(null)) return;
        switch (item){
            case "du jour":
                Double saturation = getDaySaturation();
                risk = toRisk(saturation);
                break;
            case "du lendemain":
                saturation = getNextDaySaturation();
                risk = toRisk(saturation);
                break;
            case "de la semaine prochine":
                saturation = getNextWeekSaturation();
                risk = toRisk(saturation);
                break;
            case "du mois prochain":
                saturation = getNextMonthSaturation();
                risk = toRisk(saturation);
                break;
        }
        setRiskBar(risk);
    }

    public void deleteSelectedRow(ActionEvent event) {
        Event selectedItem = eventTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Boolean res = deleteEvent(selectedItem);
            if(res) {eventsList.remove(selectedItem);
            }else {
                setErrorLabel(deleteEventErrorLabel, "erreur durant la suppression",3);
            }
        }else {
            setErrorLabel(deleteEventErrorLabel, "aucun élement selectioner",3);
        }
    }
}
