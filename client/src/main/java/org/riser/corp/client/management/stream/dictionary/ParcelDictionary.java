package org.riser.corp.client.management.stream.dictionary;

public final class ParcelDictionary {
    public static String
            INSERT = "insert_ambulance",
            UPDATE = "update_ambulance",
            PRINT = "print_ambulance",
            PRINT_ALL = "print_all_ambulances";

    //Enums of non variable parcel components
    public enum UseCase{
        NONE,
        EMERGENCY,
        AMBULANCE
    }


    public enum NextPeriod{
        NEXT_HOUR,
        NEXT_DAY,
        NEXT_WEEK,
        NEXT_MONTH
    }

}
