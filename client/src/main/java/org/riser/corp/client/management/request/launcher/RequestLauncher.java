package org.riser.corp.client.management.request.launcher;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.riser.corp.client.management.stream.parcel.Request;
import org.riser.corp.client.management.stream.parcel.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

import static org.riser.corp.client.management.stream.parcel.ParcelParser.*;
import static org.riser.corp.client.system.Core.READER;
import static org.riser.corp.client.system.Core.WRITER;

public final class RequestLauncher {
    final static Logger LOGGER = LogManager.getLogger(RequestLauncher.class);
    public static synchronized Response LaunchRequest(Request request) {
            String response;
            try {
                LOGGER.info("The request is " + request);
                WRITER.write(RequestToJSON(request));
                WRITER.write(RequestToJSON(request));
                WRITER.newLine();
                WRITER.flush();
                response = READER.readLine();
                LOGGER.info("The response is : " + response);
                return JSONToResponse(response);
            } catch(JsonProcessingException e) {
                LOGGER.error(e.getMessage());
            } catch(IOException e) {
                LOGGER.error(e.getMessage());
            }
            return null;
        }





}
