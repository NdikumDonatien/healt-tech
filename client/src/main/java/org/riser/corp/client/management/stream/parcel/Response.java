package org.riser.corp.client.management.stream.parcel;

import org.riser.corp.client.management.stream.dictionary.ParcelDictionary.*;

//Response is the pattern to build every response sent to the client
public class Response extends Parcel {
    private String message;//Message sent by server, maybe due to error cases

    public Response() {
    }

    public Response(UseCase useCase, String method, Object object) {
        super (useCase, method, object);
    }
    public Response(UseCase useCase, String method, String message){
        super (useCase, method);
        this.message = message;
    }
    public Response(UseCase useCase, String method, Object result, String message) {
        super (useCase, method, result);
        this.message = message;
    }

    public Response(UseCase useCase, String method) {
        super(useCase, method);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Response{" + super.toString () +
                "message='" + message + '\'' +
                '}';
    }
}
