package org.riser.corp.client.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.riser.corp.client.dto.ambulances.Ambulance;
import org.riser.corp.client.management.request.methods.ambulances.AmbulanceRequest;
import org.riser.corp.client.management.stream.dictionary.ParcelDictionary.*;
import org.riser.corp.client.management.stream.parcel.*;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static java.time.format.DateTimeFormatter.RFC_1123_DATE_TIME;
import static org.riser.corp.client.ClientApplication.switchToScene;
import static org.riser.corp.client.management.request.launcher.RequestLauncher.LaunchRequest;
import static org.riser.corp.client.management.stream.dictionary.ParcelDictionary.*;
import static org.riser.corp.client.management.stream.parcel.ParcelParser.mapListObject;

public class AmbulanceController implements Initializable, AmbulanceRequest {
    final static Logger LOGGER = LogManager.getLogger(AmbulanceController.class);
    private final String MAT = "Numero d'immatriculation", NAME = "Nom de l'ambulance", TYPE = "Type de l'ambulance", STATE = "Etat de marche de l'ambulance", DESCRIPT = "Description de l'ambulance";
    private final String DIALOG_TITLE = "Saisissez les informations de l'ambulance à", ADD = " ajouter", MOD = " modifier";
    final static String DEBUT = "Date de début", FIN = "Date de fin", ANYONE = "Peu importe";
    final static String AMB_TRANSPORT = "Ambulance de transport", AMB_INTENSIF = "Ambulance de soins intensifs", AMB_REANIMATION = "Ambulance de reanimation", AMB_SECOURS = "Ambulance de secours", AMB_TERRAIN = "Ambulance de terrain";
    final static String DISPO = "DISPONIBLE", SERVICE = "EN SERVICE", IMM_CD = "IMMOBILISATION CD", IMM_LD = "IMMOBILISATION LD", HS = "HORS SERVICE";
    private Ambulance dialogAmbulance;
    private List<Ambulance> ambulancesList = new ArrayList<>();

    @FXML
    Label dialogTitleLabel = new Label();
    @FXML
    Button addAmbulanceButton = new Button(), updateAmbulanceButton = new Button(), avButton = new Button();
    @FXML
    TableView<Ambulance> ambulancesTable = new TableView<>();
    @FXML
    Pane menuPane = new Pane();
    @FXML
    TableColumn<Ambulance, String> registrationColumn = new TableColumn<>();
    @FXML
    TableColumn<Ambulance, String> nameColumn = new TableColumn<>();
    @FXML
    TableColumn<Ambulance, String> typeColumn = new TableColumn<>();
    @FXML
    TableColumn<Ambulance, String> stateColumn = new TableColumn<>();
    @FXML
    TableColumn<Ambulance, String> startDateColumn = new TableColumn<>();
    @FXML
    TableColumn<Ambulance, String> endDateColumn = new TableColumn<>();
    @FXML
    ChoiceBox<String> filterChoiceBox = new ChoiceBox<>(), typeChoiceBox = new ChoiceBox<>(), stateChoiceBox = new ChoiceBox<>(), avTypeBox = new ChoiceBox<>(), avStateBox = new ChoiceBox<>();
    @FXML
    TextField filterTextField = new TextField();
    @FXML
    Label infoLabel = new Label();
    @FXML
    TextField registrationTextField = new TextField(), nameTextField = new TextField(), descriptionTextField = new TextField();
    @FXML
    DatePicker startDatePicker = new DatePicker(), endDatePicker = new DatePicker();
    @FXML
    Pane dialogPane = new Pane();
    @FXML
    public Boolean manageMenu(ActionEvent event){
        try {
            boolean status = menuPane.isVisible();
            menuPane.setVisible(!status);
        } catch (Exception e){
            LOGGER.error("Can not change the visibility of the pane because " + e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @FXML
    public Boolean logOut(ActionEvent event){
        try {
            switchToScene(event, "client.fxml");
        } catch (Exception e){
            LOGGER.error("Can't log out because " + e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @FXML
    public Boolean refresh(ActionEvent event){
        try {
            Response response = AmbulanceRequest.printAllAmbulances();
            ambulancesList = mapListObject(response.getObject(), Ambulance.class);
            ObservableList<Ambulance> ambulanceObservableList = FXCollections.observableList(ambulancesList);
            ambulancesTable.setItems(ambulanceObservableList);
        } catch (Exception e){
            LOGGER.error("Can't refresh because " + e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    @FXML
    public Boolean manageDialog(ActionEvent event){
        try {
            boolean status = dialogPane.isVisible();
            if(event.getSource() == addAmbulanceButton){
                dialogTitleLabel.setText(DIALOG_TITLE + ADD);
                dialogAmbulance = new Ambulance();
                setDialogInfo(dialogAmbulance);
                dialogPane.setVisible(!status);
            }
            else if (event.getSource() == updateAmbulanceButton ){
                LOGGER.info("The selected is " + ambulancesTable.getSelectionModel().getSelectedItem());
                if(ambulancesTable.getSelectionModel().getSelectedItem() == null){infoLabel.setText("Veuillez selectionner l'ambulance à modifier");}
                else {
                    dialogTitleLabel.setText(DIALOG_TITLE + MOD);
                    dialogAmbulance = ambulancesTable.getSelectionModel().getSelectedItem();
                    dialogPane.setVisible(!status);
                    setDialogInfo(dialogAmbulance);
                    }
            } else {dialogPane.setVisible(!status);}
        } catch (Exception e){
            LOGGER.error("Can not change the visibility of the pane because " + e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    @FXML
    public Boolean submit(ActionEvent event){
        try{
            if(registrationTextField.getText() != ""){
                LOGGER.info("Submit started !");
                Ambulance ambulance = feedDialogPane();
                if (dialogTitleLabel.getText() == DIALOG_TITLE + ADD) {
                    Response response = AmbulanceRequest.insertAmbulance(ambulance);
                    if (response.getObject() == null )
                        infoLabel.setText(LocalDateTime.now() +  ": " + response.getMessage());
                    else if (Integer.valueOf(String.valueOf(response.getObject())) >= 0) {
                        dialogPane.setVisible(Boolean.FALSE);
                    } else {
                        dialogPane.setVisible(Boolean.FALSE);
                        infoLabel.setText(response.getMessage());
                    }
                }
                else if(dialogTitleLabel.getText() == DIALOG_TITLE + MOD){
                    ambulance.setId(dialogAmbulance.getId());
                    Response response = AmbulanceRequest.updateAmbulance(ambulance);
                    if (response.getObject() == Boolean.TRUE) {
                        dialogPane.setVisible(Boolean.FALSE);
                    } else {
                        LOGGER.error("Problème de mise à jour");
                        dialogPane.setVisible(Boolean.FALSE);
                        infoLabel.setText(response.getMessage());
                    }
                }
            }
        } catch (Exception e){
            LOGGER.error("Impossible d'insérer car " + e);
            return Boolean.FALSE;
        } finally {
            refresh(new ActionEvent());
        }
        return Boolean.TRUE;
    }

    @FXML
    public String countAmbNumber(ActionEvent event){
        Response response = AmbulanceRequest.printAllAmbulances();
        ambulancesList = mapListObject(response.getObject(), Ambulance.class);
        String type = avTypeBox.getValue(), state = avStateBox.getValue();
        if(type == null || type.equals(ANYONE)){
            if(state == null || state.equals(ANYONE)){
                avButton.setText(String.valueOf(ambulancesList.size()));
            }
            else{
                avButton.setText(String.valueOf(ambulancesList.stream().filter(ambulance -> Objects.equals(ambulance.getState(), state)).collect(Collectors.toSet()).size()));
            }
        }
        else {
            if (state != null && !state.equals(ANYONE))
                avButton.setText(String.valueOf(ambulancesList.stream().filter(ambulance -> Objects.equals(ambulance.getState(), state) && Objects.equals(ambulance.getType(), type)).collect(Collectors.toSet()).size()));
            else
                avButton.setText(String.valueOf(ambulancesList.stream().filter(ambulance -> Objects.equals(ambulance.getState(), type)).collect(Collectors.toSet()).size()));
        }
        return String.valueOf(avButton.getText());
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> typeList = FXCollections.observableArrayList(AMB_TRANSPORT, AMB_INTENSIF, AMB_REANIMATION, AMB_SECOURS, AMB_TERRAIN);
        typeChoiceBox.setItems(typeList);
        ObservableList<String> statusList = FXCollections.observableArrayList(DISPO, SERVICE, IMM_CD, IMM_LD, HS);
        stateChoiceBox.setItems(statusList);
        ObservableList<String> filterList = FXCollections.observableArrayList(MAT, NAME, STATE, TYPE, DESCRIPT);
        filterChoiceBox.setItems(filterList);

        dialogPane.setVisible(Boolean.FALSE);

        registrationColumn.setCellValueFactory(new PropertyValueFactory<>("registration"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        stateColumn.setCellValueFactory(new PropertyValueFactory<>("state"));
        startDateColumn.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        endDateColumn.setCellValueFactory(new PropertyValueFactory<>("endDate"));

        ObservableList<String> avStatusList = FXCollections.observableArrayList(ANYONE, DISPO, SERVICE, IMM_CD, IMM_LD, HS), avTypeList = FXCollections.observableArrayList(ANYONE, AMB_TRANSPORT, AMB_INTENSIF, AMB_REANIMATION, AMB_SECOURS, AMB_TERRAIN);
        avStateBox.setItems(avStatusList);
        avTypeBox.setItems(avTypeList);


        refresh(new ActionEvent());
    }
    private Boolean setDialogInfo(Ambulance ambulance){
        try {
            registrationTextField.setText(ambulance.getRegistration());
            nameTextField.setText(ambulance.getName());
            typeChoiceBox.setValue(ambulance.getType());
            stateChoiceBox.setValue(ambulance.getState());
            startDatePicker.setValue(ambulance.getStartDate());
            endDatePicker.setValue(ambulance.getEndDate());
            descriptionTextField.setText(ambulance.getDescription());
            return Boolean.TRUE;
        } catch (Exception e){
            LOGGER.error("Can't set dialog informations because " + e);
            return Boolean.FALSE;
        }
    }
    private Ambulance feedDialogPane(){
        try {
            if (registrationTextField.getText() != "") {
                Ambulance ambulance = new Ambulance();
                ambulance.setRegistration(registrationTextField.getText());
                if (nameTextField.getText() != "") {
                    ambulance.setName(nameTextField.getText());
                }
                if (typeChoiceBox.getValue() != null) {
                    ambulance.setType(typeChoiceBox.getValue());
                }
                if (stateChoiceBox.getValue() != null) {
                    ambulance.setState(stateChoiceBox.getValue());
                }
                if (startDatePicker.getValue() != null) {
                    ambulance.setStartDate(startDatePicker.getValue());
                }
                if (endDatePicker.getValue() != null) {
                    ambulance.setEndDate(endDatePicker.getValue());
                }
                if (descriptionTextField.getText() != "") {
                    ambulance.setDescription(descriptionTextField.getText());
                }
                return ambulance;
            } else {
                return null;
            }
        } catch (Exception e){
            LOGGER.error("Can't feed the dialog pane because : " + e);
        }
        return null;
    }
}
