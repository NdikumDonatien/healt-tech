package org.riser.corp.client;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.riser.corp.client.controller.ClientController;

import java.io.IOException;

public class ClientApplication extends Application {
    final static Logger LOGGER = LogManager.getLogger(ClientApplication.class);
    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load (ClientApplication.class.getResource ("client.fxml"));
        Scene scene = new Scene (root);
        stage.setTitle ("SERVICE SANTE");
        stage.setScene (scene);
        stage.setResizable (false);
        stage.show ();
    }
    public static void switchToScene(ActionEvent event, String name) {
        try {
            Parent root = FXMLLoader.load (ClientApplication.class.getResource (name));
            Stage stage = (Stage)((Node)event.getSource ()).getScene ().getWindow ();
            Scene scene = new Scene (root);
            stage.setScene (scene);
            stage.setResizable(true);
            stage.show ();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public static void main(String[] args) {
        launch ();
    }
}