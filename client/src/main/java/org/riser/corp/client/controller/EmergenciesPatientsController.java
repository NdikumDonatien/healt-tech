package org.riser.corp.client.controller;


import javafx.scene.control.*;

import org.riser.corp.client.dto.emergencies.Patient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static org.riser.corp.client.ClientApplication.switchToScene;
import static org.riser.corp.client.management.request.methods.EmergenciesPatientsRequests.*;
import static org.riser.corp.client.controller.ClientController.*;
import static org.riser.corp.client.util.DateConverter.convertDatesFormat;


public class EmergenciesPatientsController implements Initializable {
    @FXML
    private Label profile;
    @FXML
    private TableView<Patient> patientTable;
    @FXML
    private TableColumn<Patient, String> securityNumber, admissionDate, releaseDate;
    @FXML
    private TextField secuField;

    private ObservableList<Patient> patientsList;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        profile.setText(pofileValue);

        securityNumber.setCellValueFactory (new PropertyValueFactory<> ("secNumber"));
        admissionDate.setCellValueFactory (new PropertyValueFactory<> ("admissionDate"));
        releaseDate.setCellValueFactory (new PropertyValueFactory<> ("releaseDate"));
        securityNumber.setResizable (false);
        admissionDate.setResizable (false);
        releaseDate.setResizable (false);
        patientsList = FXCollections.observableArrayList();

        chargePatientTable();

    }

    private void chargePatientTable(){
        try {
            List<Patient> patients = getAllPatient();
            for (Patient patient : patients) patientsList.add (convertDatesFormat(patient, "dd-MM-yyyy"));
            patientTable.setItems (patientsList);
        }catch(Exception e){
        }
    }

    public void registerPatient(ActionEvent event){
        String secu = secuField.getText();
        if(!secu.equals(null)) {
            Patient patient = new Patient(secu);
            patient = admitPatient(patient);
            patientsList.add(patient);
        }
    }
    public void freePatient(ActionEvent event){
        Patient selectedItem = patientTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Patient patient = releasePatient(selectedItem);
            selectedItem = patient;
            }
    }

    public void switchToLog(ActionEvent event) {
        logOut(event);
    }

    public void switchToEmergencySaturation(ActionEvent event) {
        switchToScene (event, "emergency-saturation.fxml");
    }

}
