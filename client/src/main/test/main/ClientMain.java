package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.riser.corp.client.system.Core;

import java.io.IOException;

public class ClientMain {
    final static Logger LOGGER = LogManager.getLogger(ClientMain.class);
    public static void main(String[] args) throws IOException {
        new Core();
    }
}
