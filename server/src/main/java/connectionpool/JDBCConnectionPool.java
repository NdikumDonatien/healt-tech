package connectionpool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;


public enum JDBCConnectionPool {
    INSTANCE_OF_JDBC_CONNECTION_POOL;
    private ArrayList<Connection> collect = new ArrayList<Connection> ();
    private ArrayList<Connection> usedConnection = new ArrayList<>();
    private Boolean conState = false;

    private final InfoConnection infoConnection = new InfoConnection ();

    private final int INITIAL_POOL_SIZE = 5;
    private final int MAX_POOL_SIZE = 10;
    private final int MAX_TIMEOUT = 4;

    final Logger LOGGER = LogManager.getLogger (JDBCConnectionPool.class);

    JDBCConnectionPool() {
        try {
            Class.forName (infoConnection.getDriver ());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException (e);
        }

        this.createListOfCollection ();
        LOGGER.info ("Connection Pool successfully initialized!");
        conState = true;
    }

    public void createListOfCollection() {
        try {
            for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
                collect.add (DriverManager.getConnection (infoConnection.getUrl (), infoConnection.getUser (),
                        infoConnection.getPassword ()));
            }
        } catch (SQLException e) {
            LOGGER.error ("create pool of connection didn't work!");
            LOGGER.error(e.getMessage());
        }

    }

    public synchronized Connection getAConnection() {
        //if the collect is empty then the usedConnection is at 1/2 FULL
        Connection con = null;
        try {
            if (collect.isEmpty ()) {
                if (usedConnection.size () < MAX_POOL_SIZE) {
                    collect.add (DriverManager.getConnection (infoConnection.getUrl (), infoConnection.getUser (),
                            infoConnection.getPassword ()));
                } else {
                    LOGGER.warn ("Maximum pool size reached, no available connections!");
                    return null;
                }
            }
            con = collect.remove (collect.size () - 1);

            // we check if the connection we had is valid
            // method isValid() takes a max time out in second

            while (! con.isValid (MAX_TIMEOUT)) {
                con = DriverManager.getConnection (infoConnection.getUrl (),
                        infoConnection.getUser (), infoConnection.getPassword ());
            }
            usedConnection.add (con);
            LOGGER.info ("connection taken");
            connectionsInformation ();
            return con;
        } catch (SQLException e) {
            LOGGER.error (e.getMessage ());
        }
        return con;
    }

        public void releaseConnection(Connection con) {
        collect.add (con);
        usedConnection.remove (con);
        LOGGER.info ("connection released");
        connectionsInformation ();
    }

    public void closeAllConnections(){
        for (Connection c : usedConnection) {
            this.releaseConnection (c);
        }
        LOGGER.debug ("All used connections have been released");
        for (Connection c : collect) {
            try {
                c.close ();
            } catch (SQLException e) {
                LOGGER.error (e.getMessage ());
            }
        }
        LOGGER.info ("All connections have been closed");
    }

    public void connectionsInformation() {
        LOGGER.info ("The number of connection available is : " + collect.size ());
        LOGGER.info ("The number of connection on use  is : " + usedConnection.size ());
    }

    public boolean getConState(){
        return conState;
    }
}

