package connectionpool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;

public final class DataSource {
    static Logger LOGGER = LogManager.getLogger(DataSource.class);
    private static JDBCConnectionPool connectionPool;
    public DataSource(){
        connectionPool = JDBCConnectionPool.INSTANCE_OF_JDBC_CONNECTION_POOL;
    }

    public static Connection getConnection() {
      try {
          return  connectionPool.getAConnection();
      } catch(NullPointerException e){
         LOGGER.error(e.getMessage());
      }return null;
    }
    public static void releaseConnection(Connection c){
        connectionPool.releaseConnection(c);
    }
    public static void closeDataSource(){
        connectionPool.closeAllConnections();
    }
    public static Boolean getConState(){
        return connectionPool.getConState();
    }

}
