package connectionpool;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class InfoConnection {
    private static Properties properties;
    private String driver;
    private String url;
    private String user;
    private String password;

    public InfoConnection () {
        properties = new Properties ();
        InputStream input = Thread.currentThread ().getContextClassLoader ().getResourceAsStream ("connection.properties");
        try {
            properties.load (input);
            driver = properties.getProperty ("driver");
            url = properties.getProperty ("url");
            user = properties.getProperty ("user");
            password = properties.getProperty ("password");
        } catch (IOException e) {
            e.printStackTrace ();
        }
    }

    public String getDriver () {
        return driver;
    }

    public String getUrl () {
        return url;
    }

    public String getUser () {
        return user;
    }

    public String getPassword () {
        return password;
    }
}
