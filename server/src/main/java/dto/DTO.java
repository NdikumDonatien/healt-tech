package dto;

import java.io.Serializable;

//All transferable object has an id in database and is serializable
public abstract class DTO implements Serializable {
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
