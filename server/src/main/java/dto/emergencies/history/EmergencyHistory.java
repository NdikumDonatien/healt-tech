package dto.emergencies.history;

import dto.DTO;

import java.time.LocalDate;

import static util.EmergencyUsefulMethods.getCurrentDate;

public class EmergencyHistory extends DTO {
    private LocalDate ehDate;
    private Integer maxDayPatients;
    private Double daySaturation;
    private Double dayRisk;

    public EmergencyHistory() {
    }

    public EmergencyHistory(Integer maxDayPatients, Double daySaturation, Double dayRisk) {
        this.ehDate = getCurrentDate();
        this.maxDayPatients = maxDayPatients;
        this.daySaturation = daySaturation;
        this.dayRisk = dayRisk;
    }

    public LocalDate getEhDate() {
        return ehDate;
    }

    public void setEhDate(LocalDate ehDate) {
        this.ehDate = ehDate;
    }

    public Integer getMaxDayPatients() {
        return Integer.valueOf(maxDayPatients);
    }

    public void setMaxDayPatients(Integer maxDayPatients) {
        this.maxDayPatients = maxDayPatients;
    }

    public Double getDaySaturation() {
        return daySaturation;
    }

    public void setDaySaturation(Double daySaturation) {
        this.daySaturation = daySaturation;
    }

    public Double getDayRisk() {
        return dayRisk;
    }

    public void setDayRisk(Double dayRisk) {
        this.dayRisk = dayRisk;
    }

    @Override
    public String toString() {
        return "EmergencyHistory{" +
                "date='" + ehDate + '\'' +
                ", maxDayPatients=" + maxDayPatients +
                ", daySaturation=" + daySaturation +
                ", dayRisk=" + dayRisk +
                ", id=" + id +
                '}';
    }
}
