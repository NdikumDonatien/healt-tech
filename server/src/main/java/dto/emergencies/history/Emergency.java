package dto.emergencies.history;

import dto.DTO;

public class Emergency extends DTO {
    private Integer capacity;
    private Integer patientNumber;

    public Emergency() {
    }

    public Emergency(int capacity, int patients_number) {
        this.capacity = capacity;
        this.patientNumber = patients_number;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getPatientNumber() {
        return patientNumber;
    }

    public void setPatientNumber(int patientNumber) {
        this.patientNumber = patientNumber;
    }

    @Override
    public String toString() {
        return "Emergency{" +
                "capacity=" + capacity +
                ", patientsNumber=" + patientNumber +
                '}';
    }
}
