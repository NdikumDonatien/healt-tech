package dto.emergencies.history;

import dto.DTO;

public class MonthSatIndex extends DTO {
    Integer mIndex;

    public MonthSatIndex() {
    }

    public MonthSatIndex(int mIndex) {
        this.mIndex = mIndex;
    }

    public int getmIndex() {
        return mIndex;
    }

    public void setmIndex(int mIndex) {
        this.mIndex = mIndex;
    }
}
