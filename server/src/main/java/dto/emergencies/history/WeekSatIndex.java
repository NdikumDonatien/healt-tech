package dto.emergencies.history;

import dto.DTO;

public class WeekSatIndex extends DTO {
    Integer wIndex;

    public WeekSatIndex() {
    }

    public WeekSatIndex(int wIndex) {
        this.wIndex = wIndex;
    }

    public int getwIndex() {
        return wIndex;
    }

    public void setwIndex(int wIndex) {
        this.wIndex = wIndex;
    }
}
