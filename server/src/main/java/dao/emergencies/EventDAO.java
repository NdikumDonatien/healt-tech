package dao.emergencies;

import dto.emergencies.Event;

import java.util.List;

import static dao.util.DatabaseUtil.*;

public interface EventDAO {
    String EVENT_ID = "ev_id";
    String  TABLE_EVENT = "event";

    static int registerEvent(Event event) {
        int id = create (TABLE_EVENT, event);
        return id;
    }
    static List<Event> getAllEvents() {
        List<Event> events = readAll (TABLE_EVENT, Event.class);
        return events;
    }

    static Boolean deleteEvent(Event event) {
        return delete (TABLE_EVENT, EVENT_ID, event.getId ());
    }
}
