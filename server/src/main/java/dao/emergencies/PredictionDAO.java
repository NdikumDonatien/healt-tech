package dao.emergencies;

import dto.emergencies.history.EmergencyHistory;
import dto.emergencies.history.MonthSatIndex;
import dto.emergencies.history.WeekSatIndex;

import java.util.Collections;
import java.util.List;

import static dao.util.DatabaseUtil.create;
import static dao.util.DatabaseUtil.readAll;

public final class PredictionDAO {

    private static final String

            EMERGENCY_HISTORY_ID = "eh_id",
            M_ID = "m_id",
            W_ID = "w_id",
            TABLE_HISTORY = "emergency_history",
            TABLE_M_INDEX = "month_sat_index",
            TABLE_W_INDEX = "week_sat_index";

    public static int createNewEmergencyHistory(EmergencyHistory history) {
        int id = create(TABLE_HISTORY, history);
        return id;
    }

    public static List<EmergencyHistory> getLastEmergenciesHistory(Integer limit){
        List<EmergencyHistory> list;
        list = readAll(TABLE_HISTORY, EmergencyHistory.class, true, EMERGENCY_HISTORY_ID, limit);
        Collections.reverse(list);
        return list;
    }
    public static List<MonthSatIndex> getLastMonthsIndex(){
        List<MonthSatIndex> list;
        list = readAll(TABLE_M_INDEX, MonthSatIndex.class, true, M_ID, 3);
        Collections.reverse(list);
        return list;
    }

    public static List<WeekSatIndex> getLastWeeksIndex(){
        List<WeekSatIndex> list;
        list = readAll(TABLE_W_INDEX, WeekSatIndex.class, true, W_ID, 5);
        Collections.reverse(list);
        return list;
    }

    public static int registerMIndex(MonthSatIndex m){
        return create(TABLE_M_INDEX, m);
    }
    public static int registerWIndex(WeekSatIndex w){
        return create(TABLE_W_INDEX, w);
    }


}
