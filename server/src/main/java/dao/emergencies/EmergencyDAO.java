package dao.emergencies;

import dto.emergencies.history.Emergency;

import static dao.util.DatabaseUtil.read;
import static dao.util.DatabaseUtil.update;

public interface EmergencyDAO {
    String EMERGENCY_ID = "em_id";
    String TABLE_EMERGENCY = "emergency";
    String SUCCESS = "Success!";
    String FAILURE = "Failure!";

    static Emergency getEmergency() {
        return read(TABLE_EMERGENCY, EMERGENCY_ID, 1, Emergency.class);
    }

    static String updateEmergency(Emergency emergency) {
        Boolean bool = update(TABLE_EMERGENCY, EMERGENCY_ID , emergency);
        if(bool){
            return SUCCESS;
        }
        else {
            return FAILURE;
        }
    }
}
