package dao.emergencies;

import dto.emergencies.Patient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static dao.util.DatabaseUtil.*;
import static services.prediction.material.Data.decrementNumberOfPatient;
import static services.prediction.material.Data.incrementNumberOfPatient;


public interface PatientDAO {
    Logger LOGGER = LogManager.getLogger(PatientDAO.class);
    String PATIENT_ID = "pat_id";
    String TABLE_PATIENT = "patient";
    String SUCCESS = "Success!";
    String FAILURE = "Failure!";

    static int registerPatient(Patient patient)  {
        int generatedId = create(TABLE_PATIENT, patient);
        incrementNumberOfPatient();
        return generatedId;
    }

    static String releasePatient(Patient patient) {
        Boolean result = update(TABLE_PATIENT, PATIENT_ID, patient);
        decrementNumberOfPatient();
        if (result.equals(true)){
            return SUCCESS;
        }else {
            return FAILURE;
        }
    }

    static List<Patient> getAllPatient() {
        return readAll(TABLE_PATIENT, Patient.class);
    }


}
