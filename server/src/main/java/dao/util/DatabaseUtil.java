package dao.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static connectionpool.DataSource.getConnection;
import static connectionpool.DataSource.releaseConnection;

public final class DatabaseUtil {
    final static Logger LOGGER = LogManager.getLogger(DatabaseUtil.class);

    //Method to create a new object in database (can be use for any object)
    public static int create(String tableName, Object object) {
        try {
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append(buildInsertStatement(tableName, object));

            sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);
            sqlBuilder.append(")");

            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString(), Statement.RETURN_GENERATED_KEYS);
            //Inserting values
            int i = 1;
            for(Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Object value = field.get(object);
                // Conversion java.util.Date en java.sql.Date si nécessaire

                preparedStatement.setObject(i, value);
                i++;
            }

            LOGGER.info(preparedStatement.toString());

            int affectedRows = preparedStatement.executeUpdate();

            releaseConnection(connection);

            if(affectedRows == 0) {
                throw new SQLException("Insertion Failed! No modification observed.");
            }
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if(generatedKeys.next()) {
                LOGGER.info("generated: " + generatedKeys.getInt(1));
                return generatedKeys.getInt(1);
            } else {
                throw new SQLException("Insertion Failed! No generated Id detected.");
            }
        } catch(SQLException sqle) {
            LOGGER.error(sqle.getMessage());
        } catch(IllegalAccessException iex) {
            LOGGER.error(iex.getMessage());
        }
        return - 1;
    }

    // Method to read an object from database (can be use for any object)
    public static <T> T read(String tableName, String idColumn, int id, Class<T> clazz) {
        try {
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append("SELECT * FROM ").append(tableName).append(" WHERE ").append(idColumn).append(" = ?");
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString());
            preparedStatement.setInt(1, id);
            LOGGER.info(preparedStatement.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            releaseConnection(connection);

            return createObjectFromResultSet(resultSet, clazz);
        } catch (SQLException ex) {
            // Handle SQLException
            LOGGER.error(ex.getMessage());
        } catch (InvocationTargetException ex) {
            // Handle InvocationTargetException
            LOGGER.error(ex.getMessage());
        } catch (InstantiationException ex) {
            // Handle InstantiationException
            LOGGER.error(ex.getMessage());
        } catch (IllegalAccessException ex) {
            // Handle IllegalAccessException
            LOGGER.error(ex.getMessage());
        } catch (NoSuchMethodException ex) {
            // Handle NoSuchMethodException
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    // Method to read all objects from database (can be use for any object) without options
    public static <T> List<T> readAll(String tableName, Class<T> clazz){
        return  readAll(tableName, clazz, false,"", 0);
    }

    // Method to read all objects from database (can be use for any object) with options
    public static <T> List<T> readAll(String tableName, Class<T> clazz, Boolean OD, String idColum, int limit) {
        List<T> resultList = new ArrayList<>();
        try {
            String statement = buildReadAllStatement(tableName, OD, idColum, limit);
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(statement);
            LOGGER.info(preparedStatement.toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            releaseConnection(connection);
            resultList = createListOfObjectsFromResultSet(resultSet, clazz);
        } catch(SQLException ex) {
            // Handle InvocationTargetException
            LOGGER.error(ex.getMessage());
        } catch(IllegalAccessException ex) {
            // Handle IllegalAccessException
            LOGGER.error(ex.getMessage());
        } catch(InstantiationException | NoSuchMethodException | InvocationTargetException ex) {
            LOGGER.error(ex.getMessage());
        }
        return resultList;
    }

    // Method to update an object in database (can be use for any object)
    public static Boolean update(String tableName, String idColumn, Object object) {
        try {
            Field[] fields = object.getClass().getDeclaredFields();
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(buildUpdateStatement(tableName, idColumn, object));
            LOGGER.info(preparedStatement.toString());

            Field idField = object.getClass().getSuperclass().getDeclaredField("id");
            idField.setAccessible(true);
            Object id = idField.get(object);
            int i = 1;
            for(Field field : fields) {
                field.setAccessible(true);
                Object value = field.get(object);
                if (value instanceof java.util.Date) {
                    value = new java.sql.Date(((java.util.Date) value).getTime());
                }
                preparedStatement.setObject(i, value);
                i++;
            }
            preparedStatement.setObject(fields.length + 1, id);
            LOGGER.info(preparedStatement.toString());

            int affectedRows = preparedStatement.executeUpdate();
            releaseConnection(connection);

            if(affectedRows == 0) {
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        } catch (SQLException ex) {
            // Handle InvocationTargetException
            LOGGER.error(ex.getMessage());

        } catch (IllegalAccessException ex) {
            // Handle IllegalAccessException
            LOGGER.error(ex.getMessage());
        } catch (NoSuchFieldException ex) {
            // Handle NoSuchMethodException
            LOGGER.error(ex.getMessage());
        }
        return Boolean.FALSE;
    }


    // Method to delete an object in database (can be use for any object)
    public static Boolean delete(String tableName, String idColumn, int id) {
        try {
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append("DELETE FROM ").append(tableName).append(" WHERE ").append(idColumn).append(" = ?");
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sqlBuilder.toString());
            preparedStatement.setInt(1, id);

            int affectedRows = preparedStatement.executeUpdate();
            releaseConnection(connection);

            if(affectedRows == 0) {
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        } catch(SQLException ex) {
            LOGGER.error(ex.getMessage());
        }
        return Boolean.FALSE;
    }

    //Use a result set to create an object (the object attributes must be same as one in database)
    private static <T> T createObjectFromResultSet(ResultSet resultSet, Class<T> clazz) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, SQLException {
        T object = clazz.getDeclaredConstructor().newInstance();
        ResultSetMetaData metaData = resultSet.getMetaData();

        int columnCount = metaData.getColumnCount();
        LOGGER.info("parsing object number of column: " + columnCount);

        if(resultSet.next()) {
            for(int i = 1; i <= columnCount; i++) {
                String columnName = oracleToCamelCase(metaData.getColumnName(i));
                LOGGER.info("parsing object name: " + columnName);
                Object value = resultSet.getObject(metaData.getColumnName(i));
                try {
                    LOGGER.info(value.toString());
                }catch(NullPointerException e){
                    LOGGER.info("Null value");
                }
                if(i == 1) {
                    try {
                        Field field = clazz.getSuperclass().getDeclaredField("id");
                        LOGGER.info(field.getName());
                        field.setAccessible(true);
                        field.set( object, value );
                        LOGGER.info(object.toString());
                    } catch (NoSuchFieldException e) {
                        throw new SQLException("Read id failed!");
                    }
                } else {
                    try {
                        LOGGER.info("trying");
                        Field field = clazz.getDeclaredField(columnName);
                        LOGGER.info(field.getName());
                        field.setAccessible(true);
                        if (field.getType() == java.time.LocalDate.class) {
                            // Convert java.sql.Date to java.time.LocalDate
                            try {
                                java.sql.Date sqlDateValue = (java.sql.Date) value;
                                LocalDate localDateValue = sqlDateValue.toLocalDate();
                                field.set( object, localDateValue );
                            }catch(Exception e){}
                        }else {
                            field.set( object, value );
                        }
                    } catch (NoSuchFieldException e) {
                        throw new SQLException("Read failed!");
                    }
                }
            }
        }
        LOGGER.info("Finish building");
        LOGGER.info("parsed object: " + object.toString());
        return object;
    }

    //Use a result set to create a list of objects (the object attributes must be same as one in database)
    private static <T> List<T> createListOfObjectsFromResultSet(ResultSet resultSet, Class<T> clazz) throws SQLException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<T> list = new ArrayList<>();
        while(resultSet.next()) {
            LOGGER.info(" step");
            T object = clazz.getDeclaredConstructor().newInstance();
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            for(int i = 1; i <= columnCount; i++) {
                String columnName = oracleToCamelCase(metaData.getColumnName(i));
                LOGGER.info("parsing object: " + columnName);
                Object value = resultSet.getObject(metaData.getColumnName(i));
                try {
                    LOGGER.info(value.toString());
                }catch(NullPointerException e){
                    LOGGER.info("null value");
                }
                   if(i == 1) {
                    try {
                        Field field = clazz.getSuperclass().getDeclaredField("id");
                        LOGGER.info(field.getName());
                        field.setAccessible(true);
                        field.set(object, value);
                        LOGGER.info(object.toString());
                    } catch(Exception e) {}
                } else {
                    try {
                        LOGGER.info("trying");
                        Field field = clazz.getDeclaredField(columnName);
                        LOGGER.info(field.getName());
                        field.setAccessible(true);
                        if (field.getType() == java.time.LocalDate.class) {
                            // Convert java.sql.Date to java.time.LocalDate
                            try {
                                java.sql.Date sqlDateValue = (java.sql.Date) value;
                                LocalDate localDateValue = sqlDateValue.toLocalDate();
                                field.set( object, localDateValue );
                            }catch(Exception e){
                                LOGGER.error("There's a problem because " + e);
                            }
                        }else {
                            field.set( object, value );
                        }
                    } catch(NoSuchFieldException e) {
                        throw new SQLException("Read failed! because " + e);
                    }
                }
            }
            list.add(object);
        }

        return list;
    }

    //method to convert an Oracle database word to camelCase format
    private static String oracleToCamelCase(String input){
        StringBuilder output = new StringBuilder();
        boolean capitalizeNext = false;

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);

            if (currentChar == '_') {
                capitalizeNext = true;
            } else {
                if (capitalizeNext) {
                    output.append(Character.toUpperCase(currentChar));
                    capitalizeNext = false;
                } else {
                    output.append(Character.toLowerCase(currentChar));
                }
            }
        }
        return output.toString();
    }

    //method to convert a camelCase word to Oracle database format
    private static String camelCaseToOracle(String input){
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);
            if (Character.isUpperCase(currentChar)) {
                output.append("_");
                output.append(Character.toLowerCase(currentChar));
            } else {
                output.append(currentChar);
            }
        }
        return output.toString();
    }

    // method to build a sql INSERT statement
    public static String buildInsertStatement(String tableName,Object object){
        StringBuilder output = new StringBuilder();
        output.append("INSERT INTO ").append(tableName).append("(");
        Field[] fields = object.getClass().getDeclaredFields();
        for(Field field : fields) {
            field.setAccessible(true);
            output.append(camelCaseToOracle(field.getName())).append(",");
        }
        output.deleteCharAt(output.length() - 1);
        output.append(") VALUES (");
        output.append("?,".repeat(fields.length));
        output.deleteCharAt(output.length() - 1);
        output.append(")");
        return output.toString();
    }

    //method to build a sql readAll statement
    public static String buildReadAllStatement(String tableName, Boolean OD, String idColum, int limit){
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT * FROM ").append(tableName);
        if(OD.equals(true)) sqlBuilder.append(" ORDER BY ").append(idColum).append(" DESC LIMIT ").append(limit);
        LOGGER.info(sqlBuilder.toString());
        return sqlBuilder.toString();
    }

    // method to build a sql UPDATE statement
    public static String buildUpdateStatement(String tableName, String idColumn, Object object){
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("UPDATE ").append(tableName).append(" SET ");

        Field[] fields = object.getClass().getDeclaredFields();
        for(Field field : fields) {
            field.setAccessible(true);
            sqlBuilder.append(camelCaseToOracle(field.getName())).append(" = ?,");
        }
        sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);
        sqlBuilder.append(" WHERE ").append(idColumn).append(" = ?");
        return sqlBuilder.toString();
    }
}
