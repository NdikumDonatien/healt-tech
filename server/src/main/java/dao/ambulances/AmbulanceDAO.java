package dao.ambulances;

import dto.ambulances.Ambulance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static connectionpool.DataSource.getConnection;
import static connectionpool.DataSource.releaseConnection;
import static dao.util.DatabaseUtil.*;


public abstract class AmbulanceDAO {
    final static Logger LOGGER = LogManager.getLogger(AmbulanceDAO.class);
    private static final String AMB = "ambulances", ID = "id", REGISTRATION = "registration", NAME = "name", STATUS = "state", TYPE = "type", DESCRIPTION = "description", START = "start_date", END = "end_date";

    public static int addAmbulance(Ambulance ambulance) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(buildInsertRequest(ambulance), Statement.RETURN_GENERATED_KEYS);
        LOGGER.info("Request : " + buildInsertRequest(ambulance));
        int affectedRows = preparedStatement.executeUpdate();
        releaseConnection(connection);
        LOGGER.info("Ambulance created");
        if (affectedRows == 0) {
            throw new SQLException("Insertion failed !");
        }
        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        if (generatedKeys.next()) {
            return generatedKeys.getInt(1);
        } else {
            throw new SQLException("Ambulance created but we can get the id.");
        }
    }

    public static List<List<?>> feedColumnsAndValues(Ambulance ambulance) throws SQLException {
        List<List<?>> listOfColumnsAndValues = new ArrayList<>();
        List<String> columns = new ArrayList<>();
        List<Object> objects = new ArrayList<>();

        if(ambulance.getRegistration() != null) {
            objects.add(ambulance.getRegistration());
            columns.add(REGISTRATION);
        } else {throw new SQLException("Pas d'immatriculation trouvé");
        }
        if(ambulance.getName() != null){
            objects.add(ambulance.getName());
            columns.add(NAME);
        }
        if(ambulance.getType() != null){
            objects.add(ambulance.getType());
            columns.add(TYPE);
        }
        if(ambulance.getState() != null){
            objects.add(ambulance.getState());
            columns.add(STATUS);
        }
        if(ambulance.getStartDate() != null){
            objects.add(ambulance.getStartDate());
            columns.add(START);
        }
        if(ambulance.getEndDate() != null){
            objects.add(ambulance.getEndDate());
            columns.add(END);
        }
        if(ambulance.getDescription() != null){
            objects.add(ambulance.getDescription());
            columns.add(DESCRIPTION);
        }
        listOfColumnsAndValues.add(columns);
        listOfColumnsAndValues.add(objects);
        return listOfColumnsAndValues;
    }
    public static String buildInsertRequest(Ambulance ambulance) throws SQLException {

        List<List<?>> listOfColumnsAndValues = feedColumnsAndValues(ambulance);
        List<String> columns = (List<String>) listOfColumnsAndValues.get(0);
        List<Object> objects = (List<Object>) listOfColumnsAndValues.get(1);
        StringBuffer request = new StringBuffer();
        request.append("INSERT INTO " + AMB + "(");
        for (String column:columns) {
            request.append(column+",");
        }
        request.deleteCharAt(request.length()-1);
        request.append(") VALUES (\'");
        for (Object object:objects) {
            request.append(object+"\',\'");
        }
        request.deleteCharAt(request.length()-1);
        request.deleteCharAt(request.length()-1);

        request.append(")");
        return request.toString();
    }

    public static Boolean updateAmbulance(Ambulance ambulance){
        Boolean result = update(AMB, ID, ambulance);
        return result;
    }
    public static List<Ambulance> printAllAmbulance(){
        List<Ambulance> ambulances = readAll(AMB, Ambulance.class);
        return ambulances;
    }
    public static Ambulance printAmbulance(Integer id){
        Ambulance ambulance = read(AMB, ID, id, Ambulance.class);
        return ambulance;
    }
}
