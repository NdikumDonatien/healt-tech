package services;

import connectionpool.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static connectionpool.DataSource.getConState;

public final class ServerService {
    final static Logger LOGGER = LogManager.getLogger(ServerService.class);
    private int PORT;
    private Boolean state;
    public static List<Socket> SOCKETS;

    public ServerService(int port) {
        //initializing variables
        SOCKETS = new ArrayList<>();
        state = true;
        PORT = port;
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(PORT);
            LOGGER.info("Starting server on port : " + PORT);
        } catch(IOException e) {
            if(state) state = false;
            LOGGER.info("Server unable to start due to port error:");
            LOGGER.error(e.getMessage());
        }

        //Initializing connection pool
        LOGGER.info("Initializing connection pool ...");

        new DataSource ();
        if(state) state = getConState();


        //Launching emergency data process
        //LOGGER.info ("EmergencyPredictionDataProcess launched!");

        //Starting the Main thread
        while (state){
            LOGGER.info("Server waiting for new connexion...");
            Socket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
            } catch(IOException e) {
                LOGGER.error(e.getMessage());
            }
            LOGGER.info("Connection to Client " + clientSocket.getInetAddress().getHostName());
            SOCKETS.add(clientSocket);
            ServerCore serverCore = new ServerCore(clientSocket);
            Thread clienThread = new Thread(serverCore);
            clienThread.start();

            //defining the ShutdownHook
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                DataSource.closeDataSource();
            }));
        }
    }
}
