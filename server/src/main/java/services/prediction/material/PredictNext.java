package services.prediction.material;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.stream.dictionary.ParcelDictionary.NextPeriod;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

import static services.prediction.material.EventsFilters.getEventsEffect;
import static util.Statistics.bestRegressionPrediction;

public final class PredictNext extends Data{

    final static Logger LOGGER = LogManager.getLogger(PredictNext.class);
    public static Double predictNextIndex(NextPeriod nextPeriod, Queue<Integer> queue){
        Double index = 0.0;
        Double extent = 1.0;
        try {
        if (nextPeriod.equals(NextPeriod.NEXT_HOUR)){
            Integer[] y =  queue.toArray(new Integer[queue.size()]);
            Integer[] x = xArray(y);
            Integer lastX = x.length;
            index = bestRegressionPrediction(x, y, lastX) * extent;
        } else if(nextPeriod.equals(NextPeriod.NEXT_DAY)) {
            Integer[] y =  queue.toArray(new Integer[queue.size()]);
            LOGGER.info("Y : " + y.length + " " + y.toString());
            Integer[] x = xArray(y);
            LOGGER.info("X : " + x.length + " " + x.toString());
            Integer lastX = x.length;
            extent += getEventsEffect("day");
            index = bestRegressionPrediction(x, y, lastX) * extent;
        }else if(nextPeriod.equals(NextPeriod.NEXT_WEEK)) {
            Integer[] y =  queue.toArray(new Integer[queue.size()]);
            Integer[] x = xArray(y);
            Integer lastX = x.length;
            extent += getEventsEffect("week");
            index = bestRegressionPrediction(x, y, lastX) * extent;
        }else if(nextPeriod.equals(NextPeriod.NEXT_MONTH)) {
            Integer[] y =  queue.toArray(new Integer[queue.size()]);
            Integer[] x = xArray(y);
            Integer lastX = x.length;
            extent += getEventsEffect("month");
            index = bestRegressionPrediction(x, y, lastX) * extent;
        }
        }catch(Exception e){}
        return index;
    }

    public static Double getNextHourSaturation(){
        return predictNextIndex(NextPeriod.NEXT_HOUR, MAX_HOUR_PATIENT_LIST);
    }
    public static Double getNextDaySaturation(){
        Queue<Integer> dpl = new ArrayDeque<> (7);
        dpl.addAll(MAX_DAY_PATIENT_LIST);
        dpl.add(Double.valueOf(predictNextIndex(NextPeriod.NEXT_DAY, MAX_DAY_PATIENT_LIST)).intValue());
        return predictNextIndex(NextPeriod.NEXT_DAY, dpl);
    }
    public static Double getNextWeekSaturation(){
        Queue<Integer> wpl = new ArrayDeque<> (5);
        wpl.addAll(MAX_WEEK_PATIENT_LIST);
        wpl.add(Double.valueOf(predictNextIndex(NextPeriod.NEXT_WEEK, MAX_WEEK_PATIENT_LIST)).intValue());
        return predictNextIndex(NextPeriod.NEXT_WEEK, wpl);
    }
    public static Double getNextMonthStturation(){
        Queue<Integer> mpl = new ArrayDeque<>(3);
        mpl.addAll(MAX_MONTH_PATIENT_LIST);
        mpl.add(Double.valueOf(predictNextIndex(NextPeriod.NEXT_MONTH, MAX_MONTH_PATIENT_LIST)).intValue());
        return predictNextIndex(NextPeriod.NEXT_MONTH, mpl);
    }


    public static Integer[] xArray(Integer[] l){
        Integer[] array = new Integer[l.length];
        Arrays.setAll(array, i -> i + 1);
        return array;
    }
}
