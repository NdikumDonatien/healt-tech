package services.prediction.material;

import dto.emergencies.Event;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class EventsFilters extends Data{

    //Separate the old events from current and upcoming ones
    public static List<Event> getPastEvents(List<Event> events) {
        LocalDate now = LocalDate.now();
        List<Event> pastEvents = new ArrayList<>();

        for (Event event : events) {
            LocalDate endDate = event.getEndDate() != null ? event.getEndDate() : null;

            if (endDate != null && (endDate.isBefore(now) || endDate.isEqual(now))) {
                pastEvents.add(event);
            }
        }

        return pastEvents;
    }

    public static List<Event> getCurrentEvents(List<Event> events) {
        LocalDate now = LocalDate.now();
        List<Event> currentEvents = new ArrayList<>();
        for (Event event : events) {
            LocalDate startDate = event.getBeginDate();
            LocalDate endDate = event.getEndDate() != null ? event.getEndDate(): null;
            if (startDate.minusDays(3).isBefore(now) && (endDate == null || endDate.isAfter(now))) {
                currentEvents.add(event);
            }
        }
        return currentEvents;
    }

    public static List<Event> getFutureEvents(List<Event> events) {
        LocalDate now = LocalDate.now();
        List<Event> futureEvents = new ArrayList<>();
        for (Event event : events) {
            LocalDate startDate = event.getBeginDate();
            if (startDate.isAfter(now) || startDate.isEqual(now)) {
                futureEvents.add(event);
            }
        }
        return futureEvents;
    }

    public static double getCumulativeEffect(List<Event> events) {
        if (events == null || events.isEmpty()) {
            return 0.0; // Si la liste est nulle ou vide, retourne 0.0
        }

        double maxEffect = Double.MIN_VALUE;
        double productSum = 0.0;

        // Trouve l'événement avec le plus grand effet
        for (Event event : events) {
            if (event.getEffect() > maxEffect) {
                maxEffect = event.getEffect();
            }
        }

        // Calcule la somme des produits des effets
        for (Event event : events) {
            productSum += event.getEffect() * maxEffect;
        }

        // Applique la formule
        return productSum + maxEffect - (maxEffect * maxEffect);
    }

    public static List<Event> getValidEvents(List<Event> events, int days) {
        List<Event> validEvents = new ArrayList<>();
        LocalDate now = LocalDate.now();
        LocalDate futureLimit = now.plusDays(days);
        for (Event event : events) {
            LocalDate startDate = event.getBeginDate();
            if (startDate.isAfter(now) || startDate.isEqual(now)) {
                if (startDate.isBefore(futureLimit)) {
                    validEvents.add(event);
                }
            }
        }
        return validEvents;
    }


    // get events effects for a valid time period
    public static Double getEventsEffect(String timeType) {
        Double effect = 0.0;
        setEventList();
        try {
            if (timeType == null) {
                // Do nothing, maybe log a message
            } else if ("day".equals(timeType)) {
                LOGGER.info("Calculating cumulative effect for day...");
                effect = getCumulativeEffect(getCurrentEvents(EVENT_LIST));
                LOGGER.info("Cumulative effect for day: " + effect);
            } else if ("week".equals(timeType)) {
                LOGGER.info("Calculating cumulative effect for week...");
                effect = getCumulativeEffect(getValidEvents(EVENT_LIST, 7));
                LOGGER.info("Cumulative effect for week: " + effect);
            } else if ("month".equals(timeType)) {
                LOGGER.info("Calculating cumulative effect for month...");
                effect = getCumulativeEffect(getValidEvents(EVENT_LIST, 30));
                LOGGER.info("Cumulative effect for month: " + effect);
            }
        } catch (Exception e) {
            LOGGER.error("An error occurred: " + e.getMessage());
        }
        return effect;
    }

}
