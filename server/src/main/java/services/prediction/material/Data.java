package services.prediction.material;

import dao.emergencies.EmergencyDAO;
import dto.emergencies.Event;
import dto.emergencies.history.Emergency;
import dto.emergencies.history.EmergencyHistory;
import dto.emergencies.history.MonthSatIndex;
import dto.emergencies.history.WeekSatIndex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.stream.dictionary.ParcelDictionary.NextPeriod;

import java.util.*;

import static dao.emergencies.EmergencyDAO.getEmergency;
import static dao.emergencies.EmergencyDAO.updateEmergency;
import static dao.emergencies.EventDAO.getAllEvents;
import static dao.emergencies.PredictionDAO.*;
import static services.prediction.material.PredictNext.predictNextIndex;

public class Data {
    final static Logger LOGGER = LogManager.getLogger(Data.class);

    protected static Emergency EMERGENCY;
    public static Integer MAX_DAY_PATIENTS;
    public static Double DAY_RISK;
    public static Double DAY_SATURATION;

    public static Queue<Integer> MAX_HOUR_PATIENT_LIST = new ArrayDeque<> (6);
    public static Queue<Integer> MAX_DAY_PATIENT_LIST = new ArrayDeque<> (7);
    public static Queue<Integer> MAX_WEEK_PATIENT_LIST = new ArrayDeque<> (5);
    public static Queue<Integer> MAX_MONTH_PATIENT_LIST = new ArrayDeque<> (3);
    public static List<Event> EVENT_LIST = new ArrayList<>();

    public static void InitializeEmergencyPredictData(){
        setEmergency();
        setEventList();
        setMaxDayPatientList();
        setMaxWeekPatientList();
        setMaxMonthPatientList();
        setDaySaturation();
        setDayRisk();
    }

    //get emergency features from database
    public static void setEmergency() {
        EMERGENCY = getEmergency();
    }
    //get events features from database
    public static void setEventList(){
        EVENT_LIST = getAllEvents();
    }
    public static void setMaxDayPatientList(){
        List<EmergencyHistory> mdList = getLastEmergenciesHistory(7);
        for(EmergencyHistory v : mdList) MAX_DAY_PATIENT_LIST.add(v.getMaxDayPatients());
        LOGGER.info("MAX_DAY_PATIENT_LIST : " + MAX_DAY_PATIENT_LIST);
    }
    public static void setMaxWeekPatientList(){
        List<WeekSatIndex> mwList = getLastWeeksIndex();
        for(WeekSatIndex v : mwList) MAX_WEEK_PATIENT_LIST.add(v.getwIndex());
        LOGGER.info("MAX_WEEK_PATIENT_LIST : " + MAX_WEEK_PATIENT_LIST);
    }
    public static void setMaxMonthPatientList(){
        List<MonthSatIndex> mmList = getLastMonthsIndex();
        for(MonthSatIndex v : mmList) MAX_MONTH_PATIENT_LIST.add(v.getmIndex());
        LOGGER.info("MAX_MONTH_PATIENT_LIST : " + MAX_MONTH_PATIENT_LIST);
    }
    public static void setDaySaturation(){
        try {
            DAY_SATURATION = predictNextIndex(NextPeriod.NEXT_DAY, MAX_DAY_PATIENT_LIST);
            LOGGER.info("DAY_RISK : " + DAY_SATURATION);
        }catch(Exception e){}
    }
    public static void setDayRisk(){
        try {
            DAY_RISK = DAY_SATURATION /EMERGENCY.getCapacity();
            LOGGER.info("DAY_SATURATION : " + DAY_RISK);
        }catch(Exception e){}
    }

    public static void incrementNumberOfPatient(){
        setEmergency();
        EMERGENCY.setPatientNumber(EMERGENCY.getPatientNumber() + 1);
        if (MAX_DAY_PATIENTS < EMERGENCY.getPatientNumber()){
            MAX_DAY_PATIENTS = EMERGENCY.getPatientNumber();
        }
        updateEmergency(EMERGENCY);
    }

    public static void decrementNumberOfPatient(){
        LOGGER.info( "decrementing" );
        setEmergency();
        EMERGENCY.setPatientNumber(EMERGENCY.getPatientNumber() - 1);
        EmergencyDAO.updateEmergency(EMERGENCY);
    }



}
