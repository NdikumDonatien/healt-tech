package services.prediction.process;

import dto.emergencies.history.EmergencyHistory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static dao.emergencies.PredictionDAO.createNewEmergencyHistory;
import static services.prediction.material.Data.*;

public final class EmergencyPredictionDataProcess {
    final static Logger LOGGER = LogManager.getLogger(EmergencyPredictionDataProcess.class);
    public EmergencyPredictionDataProcess() {
        LOGGER.info ("Starting EmergencyPredictionDataProcess ...");
        InitializeEmergencyPredictData();
        Timer timer = new Timer ();
        Calendar dayCalendar = Calendar.getInstance ();
        dayCalendar.set (Calendar.HOUR_OF_DAY, 23);
        dayCalendar.set (Calendar.MINUTE, 55);
        dayCalendar.set (Calendar.SECOND, 0);
        Date date = dayCalendar.getTime ();

        timer.schedule (new RegisterHistory (), date, 24 * 60 * 60 * 1000);

        Calendar hourCalendar = Calendar.getInstance ();
        hourCalendar.set (Calendar.MINUTE, 0);
        hourCalendar.set (Calendar.SECOND, 0);
        hourCalendar.set (Calendar.MILLISECOND, 0);

        timer.schedule (new RegisterHourMaxPatient (), hourCalendar.getTime (), 3600000);
    }

    //Register every day max number of patients (daily saturation index) in database
    static class RegisterHistory extends TimerTask {
        @Override
        public void run() {
            EmergencyHistory history = new EmergencyHistory(MAX_DAY_PATIENTS, DAY_RISK, DAY_SATURATION);
            createNewEmergencyHistory (history);
        }
    }

    //Register every hour max number of patients (hourly saturation index) on sever
    static class RegisterHourMaxPatient extends TimerTask{
        @Override
        public void run() {
            MAX_HOUR_PATIENT_LIST.add (MAX_DAY_PATIENTS);
        }
    }
}
