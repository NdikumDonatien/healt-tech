package services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.ClientRequestManager;

import java.io.*;
import java.net.Socket;

public final class ServerCore implements Runnable {
    final static Logger LOGGER = LogManager.getLogger(ServerCore.class);
    private Socket clientSocket;
    private boolean state = true;

    public ServerCore(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        LOGGER.info("New Thread Running! ");
        OutputStream outputStream = null;
        try {
            outputStream = clientSocket.getOutputStream();
        } catch(IOException e) {
            LOGGER.error(e.getMessage());
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        InputStream inputStream = null;
        try {
            inputStream = clientSocket.getInputStream();
        } catch(IOException e) {
            LOGGER.error(e.getMessage());
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            while (state) {
                try {
                    //getting the new request
                    String request = bufferedReader.readLine();
                    if(request.equals(null)) continue;
                    LOGGER.info("New request received: " + request);

                    //Executing request
                    String result = ClientRequestManager.executeRequest(request);
                    LOGGER.info("Found response: " + result);

                    //Sending the result to the client
                    LOGGER.info("Sending response...");

                    assert result != null;
                    bufferedWriter.write(result);
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                } catch(Exception e) {
                }
            }
        } catch(Exception e) {
            LOGGER.info("Client " + clientSocket.getInetAddress().getHostAddress() + " disconnected");
            LOGGER.error(e.getMessage());
            state = false;
        }
        ServerService.SOCKETS.remove(clientSocket);
        LOGGER.info("Thread Closed");
    }
}
