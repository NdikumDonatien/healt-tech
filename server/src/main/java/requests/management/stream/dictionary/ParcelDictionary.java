package requests.management.stream.dictionary;

public final class ParcelDictionary {

    //Enums of non variable parcel components
    public enum UseCase{
        NONE,
        EMERGENCY,
        AMBULANCE
    }


    public enum NextPeriod{
        NEXT_HOUR,
        NEXT_DAY,
        NEXT_WEEK,
        NEXT_MONTH
    }

}
