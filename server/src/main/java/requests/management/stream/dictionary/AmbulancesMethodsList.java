package requests.management.stream.dictionary;

import dto.ambulances.Ambulance;
import requests.management.stream.parcel.Request;
import requests.management.stream.parcel.Response;

import java.sql.SQLException;
import java.util.List;

import static dao.ambulances.AmbulanceDAO.*;

public interface AmbulancesMethodsList {
    String
        INSERT = "insert_ambulance",
        UPDATE = "update_ambulance",
        PRINT = "print_ambulance",
        PRINT_ALL = "print_all_ambulances";

    Response executeInsertion(Request request) throws SQLException;

    Response executeUpdate(Request request);

    Response executePrintAll();

    Response executePrint(Request request);

}
