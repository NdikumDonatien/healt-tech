package requests.management.stream.parcel;

import requests.management.stream.dictionary.ParcelDictionary.*;

import java.io.Serializable;

public  abstract class Parcel implements Serializable {
    //Parcels are transferable objects between server and client
    private UseCase useCase;//sub-request type
    private String method;//method to be called or called
    private Object object;//object to use, can be the attribute (or result) to method (called)

    public Parcel() {
    }

    public Parcel(UseCase useCase, String method, Object object) {
        this.useCase = useCase;
        this.method = method;
        this.object = object;
    }

    public Parcel(UseCase useCase, String method) {
        this.useCase = useCase;
        this.method = method;
    }

    public UseCase getUseCase() {
        return useCase;
    }

    public void setUseCase(UseCase useCase) {
        this.useCase = useCase;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        if(object != null) {
            return "Parcel{" +
                    "useCase='" + useCase + '\'' +
                    ", method='" + method + '\'' +
                    ", object=" + object.toString() +
                    '}';
        }
        else {
            return "Parcel{" +
                    "useCase='" + useCase + '\'' +
                    ", method='" + method + '\'' +
                    '}';
        }
    }
}
