package requests.management.stream.parcel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.stream.dictionary.ParcelDictionary.*;

//Request is the pattern to build every request sent to the server
public class Request extends Parcel {
    final static Logger LOGGER = LogManager.getLogger(Request.class);
    public Request(){}
    public Request(UseCase useCase, String method, Object attribute) {
        super (useCase, method, attribute);
    }
}

