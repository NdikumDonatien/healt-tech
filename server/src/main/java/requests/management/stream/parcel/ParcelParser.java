package requests.management.stream.parcel;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

//Convert the stream to object or the object to stream
public final class ParcelParser {
    static Logger LOGGER = LogManager.getLogger(ParcelParser.class);

    public static String RequestToJSON(Request request) {
        String string = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper = mapper.registerModule(new JavaTimeModule());
            string = mapper.writeValueAsString(request);
        } catch(JsonProcessingException e) {
            LOGGER.error(e.getMessage());
        }
        return string;
    }
    public static Request JSONToRequest(String json) {
        Request request = new Request();
        try {
            ObjectMapper mapper = new ObjectMapper();
            request = mapper.readValue(json, Request.class);
        } catch(JsonMappingException e) {
            LOGGER.error(e.getMessage());
        } catch(JsonParseException e) {
            LOGGER.error(e.getMessage());
        } catch(JsonProcessingException e) {
            LOGGER.error(e.getMessage());
        } catch(IOException e) {
            LOGGER.error(e.getMessage());
        }
        return request;
    }

    public static String ResponseToJSON(Response response)  {
       try {
           ObjectMapper mapper = new ObjectMapper();
           mapper = mapper.registerModule(new JavaTimeModule());
           return mapper.writeValueAsString(response);
       } catch (JsonProcessingException e) {
           LOGGER.error(e.getMessage());
       } return null;
    }
    public static Response JSONToResponse(String json) throws IOException {
        Response response = new Response();
        try {
            ObjectMapper mapper = new ObjectMapper();
            response = mapper.readValue(json, Response.class);
        } catch(JsonMappingException e) {
            LOGGER.error(e.getMessage());
        } catch(JsonParseException e) {
            LOGGER.error(e.getMessage());
        } catch(JsonProcessingException e) {
            LOGGER.error(e.getMessage());
        } catch(IOException e) {
            LOGGER.error(e.getMessage());
        }
        return response;
    }

    public static <T> T mapObject(Object object, Class<T> clazz) {
        T obj = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper = mapper.registerModule(new JavaTimeModule());
            obj = mapper.readValue(mapper.writeValueAsString(object), clazz);
        } catch(JsonMappingException e) {
            LOGGER.error(e.getMessage());
        } catch(JsonParseException e) {
            LOGGER.error(e.getMessage());
        } catch(JsonProcessingException e) {
            LOGGER.error(e.getMessage());
        } catch(IOException e) {
            LOGGER.error(e.getMessage());
        }
        return obj;
    }

    public static <T> List<T> mapListObject(Object object, Class<T> clazz ) {
        List<T> list = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JavaType javaType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
            mapper = mapper.registerModule(new JavaTimeModule());
            list = mapper.readValue(mapper.writeValueAsString(object), javaType);
        } catch(JsonMappingException e) {
            LOGGER.error(e.getMessage());
        } catch(JsonParseException e) {
            LOGGER.error(e.getMessage());
        } catch(JsonProcessingException e) {
            LOGGER.error(e.getMessage());
        } catch(IOException e) {
            LOGGER.error(e.getMessage());
        }
        return list;
    }

}
