package requests.management.subrequests.management;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.stream.parcel.Request;
import requests.management.stream.parcel.Response;


public abstract class EmergenciesRequestManager extends EmergenciesMethods {
    static Logger LOGGER = LogManager.getLogger(EmergenciesRequestManager.class);

    public static Response executeEmergenciesRequest(Request request) {
        String method = request.getMethod ();
        Response response = null;
        switch (method){
            case ADMIT_PATIENT:
                response = admitPatient(request);
                break;
            case RELEASE_PATIENT:
                response = releasePatient(request);
                break;
            case GET_ALL_PATIENTS:
                response = getAllPatient(request);
                break;
            case REGISTER_EVENT:
                response = registerEvent(request);
                break;
            case DELETE_EVENT:
                response = deleteEvent(request);
                break;
            case GET_UPCOMING_EVENTS:
                response = getUpcomingEvent(request);
                break;
            case GET_DAY_HISTORY:
                response = getDayHistory(request);
                break;
            case GET_EMERGENCY_CAPACITY:
                response = getEmergencyCapacity(request);
                break;
            case GET_NEXT_HOUR_SATURATION:
                response = getNextHourSaturation(request);
                break;
            case GET_DAY_SATURATION:
                response = getDaySaturation(request);
                break;
            case GET_NEXT_DAY_SATURATION:
                response = getNextDaySaturation(request);
                break;
            case GET_NEXT_WEEK_SATURATION:
                response = getNextWeekSaturation(request);
                break;
            case GET_NEXT_MONTH_SATURATION:
               response = getNextMonthSaturation(request);
               break;
            default:
                break;
        }
        return response;
    }
}
