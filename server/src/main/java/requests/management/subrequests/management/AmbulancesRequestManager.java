package requests.management.subrequests.management;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.stream.parcel.Request;
import requests.management.stream.parcel.Response;

public abstract class AmbulancesRequestManager extends AmbulancesMethods {
    static Logger LOGGER = LogManager.getLogger(AmbulancesRequestManager.class);

    public static Response executeAmbulancesRequest(Request request) {
        Response response = generateResponse(request);
        return response;
    }
}
