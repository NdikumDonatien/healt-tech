package requests.management.subrequests.management;

import dao.emergencies.EmergencyDAO;
import dao.emergencies.EventDAO;
import dao.emergencies.PatientDAO;
import dto.emergencies.Event;
import dto.emergencies.Patient;
import dto.emergencies.history.EmergencyHistory;
import requests.management.stream.dictionary.EmergenciesMethodsNameList;
import requests.management.stream.dictionary.ParcelDictionary;
import requests.management.stream.parcel.Request;
import requests.management.stream.parcel.Response;
import services.prediction.material.PredictNext;

import java.util.ArrayList;
import java.util.List;

import static dao.emergencies.PatientDAO.*;
import static dao.emergencies.PredictionDAO.getLastEmergenciesHistory;
import static requests.management.stream.parcel.ParcelParser.mapObject;
import static services.prediction.material.Data.MAX_WEEK_PATIENT_LIST;
import static services.prediction.material.EventsFilters.getCurrentEvents;
import static services.prediction.material.EventsFilters.getFutureEvents;
import static services.prediction.material.PredictNext.predictNextIndex;
import static util.EmergencyUsefulMethods.getCurrentDate;

public abstract class EmergenciesMethods implements EmergenciesMethodsNameList {
    public static Response admitPatient(Request request) {
        Response response = new Response (ParcelDictionary.UseCase.EMERGENCY,request.getMethod());
        Patient patient = mapObject (request.getObject (), Patient.class);
        patient.setAdmissionDate (getCurrentDate());
        int id = registerPatient(patient);
        patient.setId(id);
        response.setObject(patient);
        return response;
    }

    public static Response releasePatient(Request request) {
        Response response = new Response (ParcelDictionary.UseCase.EMERGENCY,request.getMethod());
        Patient patient = mapObject (request.getObject (), Patient.class);
        patient.setReleaseDate (getCurrentDate());
        String result = PatientDAO.releasePatient(patient);
        LOGGER.info( result );
        response.setObject(patient);
        return response;
    }

    public static Response getAllPatient(Request request) {
        Response response = new Response (ParcelDictionary.UseCase.EMERGENCY,request.getMethod());
        response.setObject(PatientDAO.getAllPatient());
        return response;    }

    public static Response registerEvent(Request request) {
        Response response = new Response (ParcelDictionary.UseCase.EMERGENCY,request.getMethod());
        Event event = mapObject(request.getObject(), Event.class);
        Integer eventid = EventDAO.registerEvent(event);
        response.setObject(eventid);
        return response;
    }

    public static Response deleteEvent(Request request) {
        Response response = new Response (ParcelDictionary.UseCase.EMERGENCY,request.getMethod());
        Event event = mapObject(request.getObject(), Event.class);
        response.setObject(EventDAO.deleteEvent(event));
        return response;
    }

    public static Response getUpcomingEvent(Request request) {
        Response response = new Response (ParcelDictionary.UseCase.EMERGENCY,request.getMethod());
        List<Event> events = EventDAO.getAllEvents();
        List<Event> upcomingEvents = new ArrayList<>();
        upcomingEvents.addAll(getCurrentEvents(events));
        upcomingEvents.addAll(getFutureEvents(events));
        response.setObject(upcomingEvents);
        return response;
    }
    public static Response getAllEvent(Request request) {
        Response response = new Response (ParcelDictionary.UseCase.EMERGENCY,request.getMethod());
        List<Event> events = EventDAO.getAllEvents();
        response.setObject(events);
        return response;
    }
    public static Response getDayHistory(Request request){
        Response response = new Response (ParcelDictionary.UseCase.EMERGENCY, request.getMethod());
        List<Integer> list = new ArrayList<>();
        List<EmergencyHistory> mdList = getLastEmergenciesHistory(14);
        for(EmergencyHistory v : mdList) list.add(v.getMaxDayPatients());
        response.setObject(list);
        return response;
    }

    public static Response getEmergencyCapacity(Request request) {
        Response response = new Response(ParcelDictionary.UseCase.EMERGENCY, request.getMethod());
        response.setObject(EmergencyDAO.getEmergency().getCapacity());
        LOGGER.info("Emergency gotten");
        return response;
    }
    public static Response getNextHourSaturation(Request request){
        Response response = new Response(ParcelDictionary.UseCase.EMERGENCY, request.getMethod());
        response.setObject(PredictNext.getNextHourSaturation());
        return response;
    }
    public static Response getDaySaturation(Request request) {
        Response response = new Response(ParcelDictionary.UseCase.EMERGENCY, request.getMethod());
        response.setObject(predictNextIndex(ParcelDictionary.NextPeriod.NEXT_WEEK, MAX_WEEK_PATIENT_LIST));
        return response;
    }

    public static Response getNextDaySaturation(Request request) {
        Response response = new Response(ParcelDictionary.UseCase.EMERGENCY, request.getMethod());
        response.setObject(PredictNext.getNextDaySaturation());
        return response;
    }
    public static Response getNextWeekSaturation(Request request) {
        Response response = new Response(ParcelDictionary.UseCase.EMERGENCY, request.getMethod());
        response.setObject(PredictNext.getNextWeekSaturation());
        return response;
    }
    public static Response getNextMonthSaturation(Request request) {
        Response response = new Response(ParcelDictionary.UseCase.EMERGENCY, request.getMethod());
        response.setObject(PredictNext.getNextMonthStturation());
        return response;
    }
}
