package requests.management.subrequests.management;

import dao.ambulances.AmbulanceDAO;
import dto.ambulances.Ambulance;
import org.apache.commons.math3.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.stream.dictionary.AmbulancesMethodsList;
import requests.management.stream.dictionary.ParcelDictionary.*;
import requests.management.stream.parcel.Request;
import requests.management.stream.parcel.Response;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static dao.ambulances.AmbulanceDAO.*;
import static dao.ambulances.AmbulanceDAO.printAmbulance;
import static java.lang.Boolean.TRUE;
import static requests.management.stream.parcel.ParcelParser.mapObject;
import static util.AmbulanceVerification.checkAmbulance;
import static util.AmbulanceVerification.refactorNameAndDescription;

public class AmbulancesMethods implements AmbulancesMethodsList {
    final static Logger LOGGER = LogManager.getLogger(AmbulancesMethods.class);
    @Override
    public Response executeInsertion(Request request) throws SQLException {
        Ambulance ambulance = mapObject(request.getObject(), Ambulance.class);
        Pair<Boolean, String> analyse = checkAmbulance(ambulance);
        if(analyse.getKey() != TRUE) {
            return new Response(UseCase.AMBULANCE, INSERT, analyse.getValue());
        }
        int result = 0;
        try {
            refactorNameAndDescription(ambulance);
            result = addAmbulance(ambulance);
        } catch (SQLException e) {
            return new Response(UseCase.AMBULANCE, INSERT, e.getMessage());
        }
        return new Response(UseCase.AMBULANCE, INSERT, Optional.of(result));
    }

    @Override
    public Response executeUpdate(Request request) {
        Ambulance ambulance = mapObject(request.getObject(), Ambulance.class);
        Pair<Boolean, String> analyse = checkAmbulance(ambulance);
        if(analyse.getKey() != Boolean.TRUE)
            return new Response(UseCase.AMBULANCE, UPDATE, analyse.getValue());
        Boolean result = null;
        try {
            refactorNameAndDescription(ambulance);
            result = updateAmbulance(ambulance);
        } catch (Exception e) {
            return new Response(UseCase.AMBULANCE, UPDATE, e.getMessage());
        }
        return new Response(UseCase.AMBULANCE, UPDATE, result);
    }

    @Override
    public Response executePrintAll() {
        List<Ambulance> ambulances = printAllAmbulance();
        return new Response(UseCase.AMBULANCE, PRINT_ALL, ambulances);
    }

    @Override
    public Response executePrint(Request request) {
        Integer id = Integer.valueOf(String.valueOf(request.getObject()));
        Ambulance ambulance = printAmbulance(id);
        return new Response(UseCase.AMBULANCE, PRINT, ambulance);
    }

    public static Response generateResponse(Request request){
        AmbulancesMethods ambulancesMethods = new AmbulancesMethods();
        switch (request.getMethod()){
            case INSERT:
                try {
                    return ambulancesMethods.executeInsertion(request);
                }catch (Exception sqlException){
                    LOGGER.error("Problem on insertion because : " + sqlException);
                }
            case UPDATE:
                return ambulancesMethods.executeUpdate(request);
            case PRINT:
                return ambulancesMethods.executePrint(request);
            case PRINT_ALL:
                return ambulancesMethods.executePrintAll();
        }
        return new Response(UseCase.AMBULANCE, INSERT, "");
    }
}
