package requests.management;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.stream.dictionary.ParcelDictionary.UseCase;
import requests.management.stream.parcel.Request;
import requests.management.stream.parcel.Response;

import static requests.management.stream.parcel.ParcelParser.JSONToRequest;
import static requests.management.stream.parcel.ParcelParser.ResponseToJSON;
import static requests.management.subrequests.management.AmbulancesRequestManager.executeAmbulancesRequest;
import static requests.management.subrequests.management.EmergenciesRequestManager.executeEmergenciesRequest;

public final class ClientRequestManager {
    final static Logger LOGGER = LogManager.getLogger(ClientRequestManager.class);

    public static String executeRequest(String requestJson) {
        Request request = JSONToRequest(requestJson);
        Response response = null;
        LOGGER.info("Finding answer to request: " + request);
        UseCase useCase = request.getUseCase();
        switch (useCase){
            case EMERGENCY:
                response = executeEmergenciesRequest(request);
                break;
            case AMBULANCE:
                response = executeAmbulancesRequest(request);
                break;
            default:
                break;
        }
        return ResponseToJSON(response);
    }

}
