package util;

import dto.ambulances.Ambulance;
import org.apache.commons.math3.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import requests.management.subrequests.management.AmbulancesMethods;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Objects;

public interface AmbulanceVerification {
    final static Logger LOGGER = LogManager.getLogger(AmbulanceVerification.class);
    String AMB_TRANSPORT = "Ambulance de transport", AMB_INTENSIF = "Ambulance de soins intensifs", AMB_REANIMATION = "Ambulance de réanimation", AMB_SECOURS = "Ambulance de secours", AMB_TERRAIN = "Ambulance de terrain";
    String DISPO = "DISPONIBLE", SERVICE = "EN SERVICE", IMM_CD = "IMMOBILISATION CD", IMM_LD = "IMMOBILISATION LD", HS = "hORS SERVICE";
    String PATTERN = "[A-Z]{2}-[0-9]{3}-[A-Z]{2}";
    int DAYS_MAX_FOR_IMM_CD = 25;

    static Pair<Boolean, String> checkAmbulance(Ambulance ambulance) {
        StringBuilder message = new StringBuilder();
        if(!checkRegistration(ambulance))
            message.append(" Mauvais format d'immatriculation,");
        if(!checkDateWithState(ambulance))
            message.append(" Vérifiez bien que : la date de fin est après la date de debut si elles existent, les immobilisation CD sont inférieurs à 25 jours");
        if(message.toString().isEmpty())
            return new Pair<>(Boolean.TRUE, "");
        else
            return new Pair<>(Boolean.FALSE, message.toString());
    }

    private static Period getPeriodOf(Ambulance ambulance){
        Period period = (ambulance.getStartDate() != null && ambulance.getEndDate() != null && checkDateLogic(ambulance)) ? Period.between(ambulance.getStartDate(), ambulance.getEndDate()) : null;
        return period;
    }
    static Boolean checkRegistration(Ambulance ambulance){
        if(ambulance.getRegistration() == null)
            return Boolean.FALSE;
        else if(ambulance.getRegistration().matches(PATTERN))
            return Boolean.TRUE;
        else
            return Boolean.FALSE;
    }
    static String refactorNameAndDescription(Ambulance ambulance){
        StringBuilder result = new StringBuilder();
        String name = (ambulance.getName() != null)?ambulance.getName():"", description = (ambulance.getDescription() != null)?ambulance.getDescription():"";
        if(name.contains("'")){
            ambulance.setName(name.replace("'", "''"));
            result.append("Name Updated + ");
        }
        if(description.contains("'")){
            ambulance.setDescription(description.replace("'", "''"));
            result.append("Description Updated");
        }
        return result.toString();
    }
    static boolean checkDateLogic(Ambulance ambulance){
        if(ambulance.getStartDate() == null && ambulance.getEndDate() == null){
            return Boolean.TRUE;
        }
        else if(ambulance.getStartDate() == null && ambulance.getEndDate() != null){
            return ambulance.getEndDate().isAfter(LocalDate.now());
        }
        else if(ambulance.getStartDate() != null && ambulance.getEndDate() == null){
            return Boolean.TRUE;
        }
        else{
            return ambulance.getStartDate().isBefore(ambulance.getEndDate());
        }
    }
    static boolean checkDateWithState(Ambulance ambulance){
        if (ambulance.getState() == null){
            return Boolean.TRUE;
        }
        else if (ambulance.getState().isBlank()){
            return Boolean.TRUE;
        }
        else if (List.of(DISPO, SERVICE).contains(ambulance.getState())) {
            Boolean logic = (ambulance.getStartDate() == null || ambulance.getStartDate().isAfter(LocalDate.now())) ? Boolean.TRUE : Boolean.FALSE;
            return checkDateLogic(ambulance) && logic;
        }
        else if (Objects.equals(ambulance.getState(), IMM_CD)){
            LOGGER.info("Verify if the period of immobilisation CD is normal...");
            return (getPeriodOf(ambulance) != null) ? getPeriodOf(ambulance).getDays() <= DAYS_MAX_FOR_IMM_CD: Boolean.TRUE;
        } else if (Objects.equals(ambulance.getState(), IMM_LD)) {
            LOGGER.info("Verify if the period of immobilisation LD is normal...");
            return (getPeriodOf(ambulance) != null) ? getPeriodOf(ambulance).getDays() > DAYS_MAX_FOR_IMM_CD: Boolean.TRUE;
        } else if (Objects.equals(ambulance.getState(), HS)) {
            return checkDateLogic(ambulance);
        }
        LOGGER.info("No status case founded");
        return Boolean.TRUE;
    }
}
