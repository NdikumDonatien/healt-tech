package util;

import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class Statistics {
    public static double bestRegressionPrediction(Integer[] x, Integer[] y, Integer lastX){
        SimpleRegression linearRegression = new SimpleRegression();
        SimpleRegression exponentialRegression = new SimpleRegression();
        SimpleRegression powerRegression = new SimpleRegression();
        SimpleRegression logarithmRegression = new SimpleRegression();

        List<SimpleRegression> regressionList = Arrays.asList(linearRegression, exponentialRegression, powerRegression, logarithmRegression);

        //Build different models
        for (int i = 0; i < x.length; i++) {
            // linear model
            linearRegression.addData(x[i], y[i]);

            // exponential model: log(y) = a + b * x
            exponentialRegression.addData(x[i], Math.log(y[i]));

            // power model : log(y) = a + b * log(x)
            powerRegression.addData(Math.log(x[i]), Math.log(y[i]));

            // logarithm model
            logarithmRegression.addData(Math.log(x[i]), y[i]);
        }

        //Find best model (the one whit greater RSquare)
        List<Double> RSquares = new ArrayList<>();
        for (SimpleRegression sr : regressionList) RSquares.add(sr.getRSquare());
        int maxRSquareIndex = RSquares.indexOf(Collections.max(RSquares));

        SimpleRegression best = regressionList.get(maxRSquareIndex);

        //Predict next value depending on the best model obtained
        switch (maxRSquareIndex){
            case 0:
                return predictLinearRegression(best, lastX);
            case 1:
                return predictExponentialRegression(best, lastX);
            case 2:
                return predictPowerRegression(best, lastX);
            case 3:
                return predictLogarithmicRegression(best, lastX);
            default:
                return 0;
        }

    }

    static double predictLinearRegression(SimpleRegression regression, double x_new) {
        return regression.getIntercept() + regression.getSlope() * x_new;
    }
    static double predictExponentialRegression(SimpleRegression regression, double x_new) {
        double alpha = regression.getSlope();
        double beta = Math.exp(regression.getIntercept());
        return beta * Math.exp(alpha * x_new);
    }

    static double predictPowerRegression(SimpleRegression regression, double x_new) {
        double beta = Math.exp(regression.getIntercept());
        double alpha = regression.getSlope();
        return beta * Math.pow(x_new, alpha);
    }

    static double predictLogarithmicRegression(SimpleRegression regression, double x_new) {
        double beta = regression.getIntercept();
        double alpha = regression.getSlope();
        return beta + alpha * Math.log(x_new);
    }

}
