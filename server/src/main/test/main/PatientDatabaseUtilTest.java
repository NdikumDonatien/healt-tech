package main;

import static org.junit.jupiter.api.Assertions.*;
import static util.EmergencyUsefulMethods.getCurrentDate;

import connectionpool.DataSource;
import dao.util.DatabaseUtil;
import dto.emergencies.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class PatientDatabaseUtilTest {

    @BeforeEach
    public void setUp() {
        new DataSource();
    }

    @Test
    public void testCreatePatient() {
        Patient patient = new Patient("1834567890");
        patient.setAdmissionDate(getCurrentDate());
        int generatedId = DatabaseUtil.create("patient", patient);
        assertTrue(generatedId > 0, "L'ID généré devrait être positif");
    }

    @Test
    public void testReadPatient() {
        int patientId = 1; // Remplacez par un ID de patient réel dans votre base de données
        Patient patient = DatabaseUtil.read("patient", "pat_id", patientId, Patient.class);
        assertNotNull(patient, "Le patient devrait être récupéré depuis la base de données");
    }

    @Test
    public void testUpdatePatient() {
        int patientId = 3; // Remplacez par un ID de patient réel dans votre base de données
        Patient patient = DatabaseUtil.read("patient", "pat_id", patientId, Patient.class);
        assertNotNull(patient, "Le patient devrait être récupéré depuis la base de données");

        patient.setSecNumber("9876543210");
        patient.setAdmissionDate(getCurrentDate());
        boolean updateResult = DatabaseUtil.update("patient", "pat_id", patient);
        assertTrue(updateResult, "La mise à jour devrait réussir");
    }

    @Test
    public void testDeletePatient() {
        int patientId = 2; // Remplacez par un ID de patient réel dans votre base de données
        boolean deleteResult = DatabaseUtil.delete("patient", "pat_id", patientId);
        assertTrue(deleteResult, "La suppression devrait réussir");
    }
}
