package main;

import static org.junit.jupiter.api.Assertions.*;
import static util.EmergencyUsefulMethods.getCurrentDate;

import connectionpool.DataSource;
import dao.util.DatabaseUtil;
import dto.emergencies.Event;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class EventDatabaseUtilTest {
    @BeforeEach
    public void setUp() {
        new DataSource();
    }

    @Test
    public void testCreate() {
        Event event = new Event( "Test Event",  getCurrentDate(), 0.5 );
        int generatedId = DatabaseUtil.create( "event", event );
        assertTrue( generatedId > 0, "L'ID généré devrait être positif" );
    }

    @Test
    public void testRead() {
        int eventId = 1;
        Event event = DatabaseUtil.read( "event", "ev_id", eventId, Event.class );
        assertNotNull( event, "L'événement devrait être récupéré depuis la base de données" );
    }

    @Test
    public void testUpdate() {
        int eventId = 1;
        Event event = DatabaseUtil.read( "event", "ev_id", eventId, Event.class );
        assertNotNull( event, "L'événement devrait être récupéré depuis la base de données" );
        event.setName( "Updated Event Name" );
        boolean updateResult = DatabaseUtil.update( "event", "ev_id", event );
        assertTrue( updateResult, "La mise à jour devrait réussir" );
    }

    @Test
    public void testDelete() {
        int eventId = 1;
        boolean deleteResult = DatabaseUtil.delete( "event", "ev_id", eventId );
        assertTrue( deleteResult, "La suppression devrait réussir" );
    }


}
